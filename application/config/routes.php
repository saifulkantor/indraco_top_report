<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['login'] = 'Home/login';
$route['logout'] = 'Home/logout';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
