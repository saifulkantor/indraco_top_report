<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$namagroup= 'SUNCITY GROUP';
$slogangroup= 'Slogan';
//$linktopreport='http://192.9.222.9:8080/Reports/Pages/Report.aspx?ItemPath=%2fsungroup%2fTop_report_tsh_scp';
if ($_SESSION['level']=='owner') {
	$semuaperusahaan = $perusahaan->getAll();
} else {
	$semuaperusahaan = $perusahaan->getMilikku($_SESSION['id_user']);
}
$semuajenis_perusahaan = $jenis_perusahaan->getAll();
$semualokasi_perusahaan = $lokasi_perusahaan->getAll();
$linktopreport = $linkTopReport;
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Indraco Top Report</title>
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/opensans-font.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/style.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/webfont/css/all.min.css" />
		<style type="text/css">
		html,body{
			height: 100%;
		}
		.page-content{
		    min-height: 100%;
		}
		.form-register .content{
			width: 100%;
			float:none;
			background: none;
		}
		.menu {
		    padding: 5px;
		}
		.menu p {
  		background: aliceblue;
	    display: block;
	    padding: 15px;
	    cursor: pointer;
		}
		.wizard-header{
			box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -o-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -ms-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -moz-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -webkit-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    margin-bottom: 20px;
			padding-top: 10px!important;
		}
		.btnback{
			position: absolute;
			font-size: 63px;
			padding: 10px;
			color: white;
			cursor:pointer;
		}
		#halamanaplikasi{
			position: absolute;
			height: 100%;
	    width: 100%;
		}
		#halamanaplikasi iframe{
			margin-top:36px;
			width: 100%;
			height: calc(100% - 36px);
		}
		.breadcrumb{
			margin: 0;
			position: absolute;
			width: 100%;
			top:0px;
			left:0px;
			height:36px;
			cursor:pointer;
			border-radius:0px;
			border-bottom:1px solid #f5f5f5;
			background: white;
			color:black;
		}
		.animation {
		    position: fixed;
		    z-index: 999;
		    -webkit-animation: ease-out normal forwards;
		    -webkit-animation-name: run;
		    -webkit-animation-duration: 0.5s;
				/* font-size: 8px; */
		}
		.animation h3 {
		    /* font-size: 10px; */
		}
		@-webkit-keyframes run {
		    0% { left:25%;width: 50%; top:20px;}
		    100%{ left:0;width:100%;top:-20px}
		}
		</style>
	</head>

	<body>
		<script type="text/javascript">
		var id_perusahaan=0;
		function frameloaded() {
			//if (id_perusahaan!=0) window.frames[0].document.querySelector('.quick-link-content a:nth-child(6)').click();
		}
		</script>
		<div id="pilihmenu" class="page-content">
			<div class="form-v1-content" style="margin-top:20px">
				<div class="wizard-form">
					<div class="form-register">
						<div class="content inner">
							<div class="wizard-header">
								<div id="btnback" class="btnback" style="display:none" onclick="pilihUnitUsaha(0)"><i class="fa fa-angle-double-left"></i></div>
								<h3 class="heading"><?=$namagroup?></h3>
								<p><?=$slogangroup?></p>
							</div>
							<div id="menureport" class="col-xs-12">
							    <div class="col-xs-12 col-sm-6 menu menuperusahaan">
									<p data-options="group:'unitusaha', toggle:true" style="background-color:#4f6dc1;color:white;" onclick="pilihTopReport('https://lapkeu.indraco-group.com/', '<< Top Report')">
										<i class="fa fa-list-alt fa-2x"></i>
										<span id="" align="left">Top Report Indraco</span>
									</p>
								</div>

								<div class="col-xs-12 col-sm-6 menu menuperusahaan">
									<p data-options="group:'unitusaha', toggle:true" style="background-color:#4f6dc1;color:white;" onclick="pilihTopReport('https://lapkeu.indraco-group.com/keuangan.php', '<< Top Report Keuangan')">
										<i class="fa fa-chart-line fa-2x"></i>
										<span id="" align="left">Top Report Keuangan</span>
									</p>
								</div>
							</div>
							<div id="menuapp" class="col-xs-12">
							    <div class="col-xs-12 col-sm-6 menu menuperusahaan">
									<p data-options="group:'unitusaha', toggle:true" style="background-color:#4f6dc1;color:white;" onclick="pilihTopReport()">
										<i class="fa fa-list-alt fa-2x"></i>
										<span id="judulreport" align="left"></span>
									</p>
								</div>
							</div>
							<div id="menuperusahaan">
								<div class="col-xs-12">
										<?php foreach ($semuaperusahaan as $key => $val) {
											echo '<div class="col-xs-12 col-sm-6 col-md-3 menu menuperusahaan">
												<p data-options="group:\'unitusaha\', toggle:true" onclick="pilihUnitUsaha('.$val->id_perusahaan.')" style="background-color:'.$val->bgcolor.';color:'.$val->color.';">
													<i class="'.$val->icon.' fa-2x"></i>
													<span align="left">'.$val->nama_perusahaan.'</span>
												</p>
											</div>';
										} ?>
										<div style="clear:both"></div>
								</div>
								<div class="col-xs-12">
									<h4>Lokasi Perusahaan</h4>
									<?php foreach ($semualokasi_perusahaan as $key => $val) {
										echo '<div style="float:left;height:20px;width:20px;background-color:'.$val->bgcolor.'">&nbsp;</div><p style="float:left;margin-right:20px"> &nbsp; '.$val->lokasi_perusahaan.'</p>';
									} ?>
								</div>
								<div class="col-xs-12">
									<h4>Jenis Perusahaan</h4>
									<?php foreach ($semuajenis_perusahaan as $key => $val) {
										echo '<div style="float:left;height:20px;width:20px;"><i class="fa '.$val->icon.'"></i></div><p style="float:left;margin-right:20px"> &nbsp; '.$val->jenis_perusahaan.'</p>';
									} ?>
								</div>
							</div>

							<div id="menusubkerja" style="display:none">
								<div class="col-xs-12">
								</div>
							</div>

						</div>

					</div>
					<div class="col-xs-12" style="position: absolute;bottom: -40px;">
						<p style="background-color: black;color: white;padding: 5px 10px;cursor: pointer;" class="pull-left" onclick="location.href='<?php echo BASE_URL; ?>/pengaturan'"><i class="fa fa-tools"></i></p>
						<p style="background-color: red;color: white;padding: 5px 20px;cursor: pointer;" class="pull-right" onclick="location.href='<?php echo BASE_URL; ?>/logout'">Logout</p>
					</div>
				</div>
			</div>
		</div>
		<div id="halamanaplikasi" style="display:none">
			<div class="breadcrumb" onclick="bukamenu()"><div style="float:left;" class="posisi"><?=$namagroup?></div></div>
			<iframe id="frame" frameborder="no" onload="frameloaded()"></iframe>
		</div>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/js/bootstrap.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery.steps.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/main.js"></script>
		<script tyle="text/javascript">
		var perusahaan=<?=json_encode($semuaperusahaan)?>;
		var listmenu=new Array();
		var menudipilih = 0;
		function bukaaplikasi(link='') {
			$('.wizard-header').addClass('animation');
			$('.content.inner').attr('style','padding-top:100px');
			// $( "#pilihmenu" ).slideUp( "slow", function() {
			$('#frame').attr('src','');
			$( "#pilihmenu" ).fadeOut( "slow", function() {
		    $('#halamanaplikasi').show();
				$('#frame').attr('src',link);
		  });
		}
		function bukamenu() {
			// $( "#pilihmenu" ).slideDown( "slow", function() {
			$('.wizard-header').removeClass('animation');
			$('.content.inner').attr('style','padding-top:0px');
			$( "#pilihmenu" ).fadeIn( "slow", function() {
		    $('#halamanaplikasi').hide();
		  });
		}

		var unit_usaha = sub_kerja = title = '';
		function pilihTopReport(url, ttl) {
			if (id_perusahaan==0) {
				<?php if ($_SESSION['level']=='owner') { ?>
				bukaaplikasi(url);
				ubahTitle();
				title = ttl;
				<?php } ?>
			} else {
				if (menudipilih!=0) {
					bukaaplikasi(menudipilih['link']);
				} else {
					alert('Masih dalam Tahap Perbaikan');
				}
			}
		}
		function pilihLink(url, name){
			if (url != '#') {
				if (unit_usaha == '') {
					$.messager.alert('Warning', 'Unit Usaha belum dipilih', 'warning');
				} else {
					sub_kerja = name;
					$('#frame').attr('src',url);

					ubahTitle();
					$('.easyui-layout').layout('collapse', 'north')
				}
			} else {
				$.messager.alert('Warning', 'Program under developing', 'warning');
				$('#frame').attr('src',url);
			}
		}
		function pilihUnitUsaha(id){
			//$('#frame').attr('src','');
			menudipilih=0;
			id_perusahaan=id;
			ubahTitle()
		}
		function ubahTitle(){
			if (id_perusahaan==0) {
				menudipilih=0;
				$('#menusubkerja').fadeOut("fast", function() {
					$('#btnback').hide();
					$('#pilihmenu .wizard-header').attr('style','background-color:none;color:black');
					$('#halamanaplikasi .breadcrumb').attr('style','background-color:none;color:black');
					$('#pilihmenu .wizard-header .heading').html('<?=$namagroup?>');
					$('#pilihmenu .wizard-header p').html('<?=$slogangroup?>');
					$('#judulreport').html('REKAP SELURUH <?=$namagroup?>');
					$('#menuapp').hide();
					$('#menureport').show();
					$('#halamanaplikasi .breadcrumb>.posisi').html('<span style="color:black">&lt;&lt;</span> <?=$namagroup?> '+title);
					$('#menureport p').attr('style','background-color:#4f6dc1;color:white;border:none;');
					$('#menuperusahaan').fadeIn("fast");
			  });
				<?php if ($_SESSION['level']!='owner') { ?>
					$('#menureport').hide();
				<?php } ?>
			} else {
				var elemen = $('#menuperusahaan');
				if ($('#menuperusahaan').is(':hidden')) elemen = $('#menusubkerja');
				elemen.fadeOut("medium", function() {
					var datadipilih = perusahaan.filter(function (per) { return per.id_perusahaan == id_perusahaan })[0];
					var id_menu = null;
					if (menudipilih!=0) {
						var nama_perusahaan = datadipilih['nama_perusahaan'];
						datadipilih = menudipilih;
						datadipilih['nama'] = datadipilih['nama_menu'];
						datadipilih['jenis'] = nama_perusahaan;
						id_menu = menudipilih['id_menu'];
						$('#judulreport').html(' Masuk ke Aplikasi '+datadipilih['nama']);
						$('#menuapp p').attr('style','background-color:'+datadipilih['bgcolor']+';color:'+datadipilih['color']+';border:1px solid black;');
						$('#menuapp p i').attr('class',datadipilih['icon']+' fa-2x');
						$('#btnback').attr('onclick','pilihUnitUsaha('+id_perusahaan+')');
						$('#menuapp').show();
						$('#menureport').hide();
						<?php if ($_SESSION['level']!='owner') { ?>
							$('#menureport').hide();
						<?php } ?>
					} else {
						datadipilih['nama'] = datadipilih['nama_perusahaan'];
						datadipilih['jenis'] = datadipilih['jenis_perusahaan'];
						$('#judulreport').html('REKAP '+datadipilih['nama']);
						$('#menuapp p').attr('style','background-color:white;color:black;border:1px solid black;');
						$('#menuapp p i').attr('class','fa fa-table fa-2x');
						$('#btnback').attr('onclick','pilihUnitUsaha(0)');
						$('#menuapp').hide();
						$('#menureport').hide();
					}
					//$('#menureport').hide();
					$('#btnback').show();
					$('#pilihmenu .wizard-header').attr('style','background-color:'+datadipilih['bgcolor']+';color:'+datadipilih['color']);
					$('#halamanaplikasi .breadcrumb').attr('style','background-color:'+datadipilih['bgcolor']+';color:'+datadipilih['color']);
					$('#pilihmenu .wizard-header .heading').html('<i class="'+datadipilih['icon']+'"></i> '+datadipilih['nama']);
					$('#pilihmenu .wizard-header p').html(datadipilih['jenis']);
					$('#halamanaplikasi .breadcrumb>.posisi').html('<span style="color:white">&lt;&lt;</span> <?=$namagroup?> &gt; <i class="'+datadipilih['icon']+'"></i> '+datadipilih['nama']);

					loadmenu_perusahaan(id_perusahaan,id_menu);
					$('#menusubkerja').fadeIn("fast");
			  });
			}
		}

		function pilihMenu(id_menu,link,nama_menu,icon_menu) {
			if (id_menu!=0) {
				var pilihan = listmenu.filter(function (men) { return men.id_menu == id_menu })[0];
				if (pilihan['anak']>0) {
					menudipilih=pilihan;
					ubahTitle();
				} else {
					var perdipilih = perusahaan.filter(function (per) { return per.id_perusahaan == id_perusahaan })[0];
					$('#halamanaplikasi .breadcrumb>.posisi').html(' <span style="color:white">&lt;&lt;</span> <?=$namagroup?> &gt; <i class="'+perdipilih['icon']+'"></i> '+perdipilih['nama_perusahaan']+' &gt; <i class="'+icon_menu+'"></i> '+nama_menu);
					bukaaplikasi(link);
				}
			} else {
				alert('Masih dalam Tahap Perbaikan');
			}
		}

		function loadmenu_perusahaan(id=0,id_parent=null) {
		   if (id!=0) {
				// $('#menusubkerja .col-xs-12').html('<img src="<?=BASE_URL?>/assets/images/loading-animation.gif" width="100%" height="100%" />');
		    var request = $.ajax({
		      url: "<?=base_url()?>ajax/menu_perusahaan",
		      method: "POST",
		      data: {
						'_csrf':'<?=$this->keamanan->generatecsrf()?>',
						'auth_key':'<?=$_SESSION['auth_key']?>',
						'id_user':'<?=$_SESSION['id_user']?>',
						'id_perusahaan':id,
						'id_parent':id_parent,
					},
		      dataType: "json"
		    });

		    request.done(function( datahasil ) {
					listmenu = new Array();
					html='';
		      $.each(datahasil,function(index,value){
						if (value['selesai']==0 || (value['keyword']==null && menudipilih==0)) {
							value['bgcolor']='white'; value['color']='black';
							value['id_menu']=0; value['link']='#';
						} else if (menudipilih!=0){
							value['link']=menudipilih['link']+'&redirect='+value['link'];
						} else {
							 value['link']+='?keyword='+value['keyword']+'&id_perusahaan='+value['id_perusahaan_aplikasi'];
						 }
						 if (value['link']!='#' || <?=($_SESSION['level']=='owner')?'true':'false'?>){
							var link = 'pilihMenu('+value['id_menu']+',\''+value['link']+'\',\''+value['nama_menu']+'\',\''+value['icon']+'\')';
							html+='<div class="col-xs-12 col-sm-6 col-md-4 menu menuperusahaan">'+
								'<p data-options="group:\'unitusaha\', toggle:true" onclick="'+link+'" style="background-color:'+value['bgcolor']+';color:'+value['color']+';border:1px solid '+value['color']+'">'+
									'<i class="'+value['icon']+' fa-2x"></i>'+
									'<span align="left"> &nbsp; '+value['nama_menu']+'</span>'+
								'</p>'+
							'</div>';
						}
						listmenu.push(value);
					});
					$('#menusubkerja .col-xs-12').html(html);
		    });

		    request.fail(function( jqXHR, textStatus ) {
		      alert( "Request failed: " + textStatus );
		    });
		  } else {
		    alert('Mohon Pilih Paling Tidak 1 Perusahaan');
		  }
		}

		window.addEventListener('DOMContentLoaded', (event) => {
			pilihUnitUsaha(0);
		});
		</script>
	</body>
</html>
