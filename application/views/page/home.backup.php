<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$dataSK = array(
	array('name'=>'Marketing', 'icon'=>'fa fa-comments-dollar', 'link'=>'#'),
	array('name'=>'Akuntansi', 'icon'=>'fab fa-accusoft', 'link'=>'http://192.9.222.61:5050/Auth/logout'),
	array('name'=>'HRD', 'icon'=>'fa fa-users', 'link'=>'#'),
	array('name'=>'GA', 'icon'=>'fab fa-superpowers', 'link'=>'#'),
	array('name'=>'Legal - Perjanjian', 'icon'=>'fa fa-file-contract', 'link'=>'#'),
	array('name'=>'Kerjasama Pihak III', 'icon'=>'fa fa-network-wired', 'link'=>'#'),
	array('name'=>'Kinerja IT', 'icon'=>'fa fa-laptop-code', 'link'=>'#'),
	array('name'=>'Guest Complain', 'icon'=>'fa fa-tired', 'link'=>'#'),
	array('name'=>'Tindak Lanjut ICA', 'icon'=>'fa fa-cogs', 'link'=>'#'),
	array('name'=>'Lain-Lain', 'icon'=>'fab fa-slack-hash', 'link'=>'#'),
	array('name'=>'Building', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('name'=>'Report', 'icon'=>'fa fa-table', 'link'=>'http://192.9.222.9:8080/Reports/Pages/Report.aspx?ItemPath=%2fsungroup%2fTop_report_tsh_scp'),
	array('name'=>'Pembebasan Lahan', 'icon'=>'fa fa-snowplow', 'link'=>'#'),
	array('name'=>'Pembangunan', 'icon'=>'fa fa-tools', 'link'=>'#'),
);
$dataUS = array(
	array('id'=>0, 'name'=>'1.A TSH SDA', 'icon'=>'fa fa-hotel', 'link'=>'#'),
	array('id'=>0, 'name'=>'1.B MALL SDA', 'icon'=>'fa fa-city', 'link'=>'#'),
	array('id'=>0, 'name'=>'1.C WP SDA', 'icon'=>'fa fa-swimming-pool', 'link'=>'#'),
	array('id'=>0, 'name'=>'2.A TSH MDN', 'icon'=>'fa fa-hotel', 'link'=>'#'),
	array('id'=>0, 'name'=>'2.B MALL MDN', 'icon'=>'fa fa-city', 'link'=>'#'),
	array('id'=>0, 'name'=>'2.C WP MDN', 'icon'=>'fa fa-swimming-pool', 'link'=>'#'),
	array('id'=>0, 'name'=>'3. SCQ BEKASI', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'4. SC RESIDENCE', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'5. SCB WUNUT', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'6. JUWET TAHAP I', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'7. PORONG', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'8. BJ BENDO', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'9. WUNUT B', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'10. KESAMBI', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'11. PBI', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'12. AB', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'13. LUMAJANG', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'14. PAGAK', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'15. JUWET TAHAP II', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'16. KEDUNG SOLO', 'icon'=>'fa fa-building', 'link'=>'#'),
	array('id'=>0, 'name'=>'17. HO', 'icon'=>'fa fa-building', 'link'=>'#'),
);
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Indraco Top Report.</title>
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/opensans-font.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/style.css" />
		<? /*
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/easyui/themes/metro-green/easyui.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/easyui/themes/icon.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/easyui/themes/color.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/webfont/css/all.min.css" />

		<link rel="stylesheet" type="text/css" href="css/opensans-font.css">
		<link rel="stylesheet" type="text/css" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		<!-- Main Style Css -->
	    <link rel="stylesheet" href="css/style.css"/>

		<script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/plugins/easyui/jquery.easyui.min.js"></script>
		<style>
		*{
			font-size:14px;
			font-family: 'Segoe UI','Microsoft Sans Serif',sans-serif;
			font-weight:450;
		}
		.fa,
		.fab,
		.fal,
		.far,
		.fas {
			width:40px;
			margin-left:15px;
		}

		.row::after {
			content: "";
			clear: both;
			display: table;
		}

		[class*="col-"] {
			float: left;
			padding: 5px;
			text-align: center;
		}

		.col-1 {width: 8.33%;}
		.col-2 {width: 16.66%;}
		.col-3 {width: 25%;}
		.col-4 {width: 33.33%;}
		.col-5 {width: 41.66%;}
		.col-6 {width: 50%;}
		.col-7 {width: 58.33%;}
		.col-8 {width: 66.66%;}
		.col-9 {width: 75%;}
		.col-10 {width: 83.33%;}
		.col-11 {width: 91.66%;}
		.col-12 {width: 100%;}
		</style>
		*/ ?>
	</head>

	<body class="easyui-layout" data-options="fit:true">
		<div style="height:100%;" data-options="region:'north',hideCollapsedContent:false" title='Top Report'>
			<div class="row">
				<span style="margin-left:10px">I. Report</span> <br>
				<div class="col-2">
					<a class="easyui-linkbutton c5" style="width:220px; height:55px; text-align:left" onclick="pilihTopReport('http://192.9.222.9:8080/Reports/Pages/Report.aspx?ItemPath=%2fsungroup%2fTop_report_tsh_scp')">
						<i class="fa fa-table fa-2x"></i>
						<span align="left">Report</span>
					</a>
				</div>
			</div>
			<div class="row">
				<span style="margin-left:10px">II. Unit Usaha</span> <br>
				<?php
					$i = 8;
					foreach($dataUS as $key => $val) :
						$i--;
						if ($i==0) $i = 8;
				?>
						<div class="col-2">
							<a class="easyui-linkbutton" style="width:220px; height:55px; text-align:left" data-options="group:'unitusaha', toggle:true" onclick="pilihUnitUsaha('<?php echo $val['id']?>', '<?php echo $val['name']?>')">
								<i class="<?php echo $val['icon']; ?> fa-2x"></i>
								<span align="left"><?php echo $val['name']; ?></span>
							</a>
						</div>
				<?php
					endforeach;
				?>
			</div>
			<div class="row">
				<span style="margin-left:10px">III. Sub Kerja</span> <br>
				<?php
					$i = 0;
					foreach($dataSK as $key => $val) :
						$i++;
						if ($i==8) $i = 1;
				?>
						<div class="col-2">
							<a class="easyui-linkbutton <?php echo 'c'.$i ?>" style="width:220px; height:55px; text-align:left" data-options="group:'subkerja', toggle:true" onclick="pilihLink('<?php echo $val['link']; ?>', '<?php echo $val['name']; ?>')">
								<span><i class="<?php echo $val['icon']; ?> fa-2x"></i></span>
								<span align="left"><?php echo $val['name']; ?></span>
							</a>
						</div>
				<?php
					endforeach;
				?>
			</div>
		</div>
		<div data-options="region:'center',title:'',border:false">
			<iframe id="myF" style="width:100%; height:99%;" frameborder="no"></iframe>
		</div>
		<script>
		var unit_usaha = sub_kerja = '';
		function pilihTopReport(url) {
			unit_usaha = sub_kerja = '';

			document.getElementById("myF").src = url;

			ubahTitle();
			$('.easyui-layout').layout('collapse', 'north')
		}
		function pilihLink(url, name){
			if (url != '#') {
				if (unit_usaha == '') {
					$.messager.alert('Warning', 'Unit Usaha belum dipilih', 'warning');
				} else {
					sub_kerja = name;
					document.getElementById("myF").src = url;

					ubahTitle();
					$('.easyui-layout').layout('collapse', 'north')
				}
			} else {
				$.messager.alert('Warning', 'Program under developing', 'warning');
				document.getElementById("myF").src = '';
			}
		}
		function pilihUnitUsaha(id, name){
			document.getElementById("myF").src = '';
			unit_usaha = name;
			sub_kerja = '';
			ubahTitle()
		}
		function ubahTitle(){
			var c = $('.easyui-layout'); // layout itself
			var w = c.layout('panel','north'); // west panel
			var str = 'Top Report';
			if (unit_usaha != '')
				str += ' > '+unit_usaha
			if (sub_kerja != '')
				str += ' > '+sub_kerja

			w.panel('setTitle', str);
			$('.panel-title').html(str)
		}
		</script>
	</body>
</html>
