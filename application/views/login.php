<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$namagroup= 'SUNCITY GROUP';
$slogangroup= 'Slogan';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Indraco Top Report.</title>
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/opensans-font.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/style.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/webfont/css/all.min.css" />
		<style type="text/css">
		html,body{
			height: 100%;
		}
		.page-content{
		    min-height: 100%;
		}
		.form-register .content{
			width: 100%;
			float:none;
			background: none;
		}
		.menu {
		    padding: 5px;
		}
		.menu p {
  		background: aliceblue;
	    display: block;
	    padding: 15px;
	    cursor: pointer;
		}
		.wizard-header{
			box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -o-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -ms-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -moz-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -webkit-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    margin-bottom: 20px;
			padding-top: 10px!important;
		}
		.btnback{
			position: absolute;
			font-size: 63px;
			padding: 10px;
			color: white;
			cursor:pointer;
		}
		#halamanaplikasi{
			position: absolute;
			height: 100%;
	    width: 100%;
		}
		#halamanaplikasi iframe{
			margin-top:36px;
			width: 100%;
			height: calc(100% - 36px);
		}
		.breadcrumb{
			margin: 0;
			position: absolute;
			width: 100%;
			top:0px;
			left:0px;
			height:36px;
			cursor:pointer;
			border-radius:0px;
			border-bottom:1px solid #f5f5f5;
			background: white;
			color:black;
		}
		.btn-primary{
			background-color: #4950a2;
			border-color: #4950a2;
		}
		.input-group-addon {
		  background-color: #4950a2;
		  color: white;
		}
		input.form-control {
	    border: 1px solid #e8e8e8;
		}
		</style>
	</head>

	<body>
		<div id="pilihmenu" class="page-content">
			<div class="form-v1-content" style="margin-top:20px">
				<div class="wizard-form">
					<div class="form-register">
						<div class="content inner">
							<div class="wizard-header">
								<div id="btnback" class="btnback" style="display:none" onclick="pilihUnitUsaha(0)"><i class="fa fa-angle-double-left"></i></div>
								<h3 class="heading">Login</h3>
								<p>Masuk untuk melanjutkan</p>
							</div>
							<div id="login" style="margin-top: 50px;">
								<div class="col-xs-12 col-md-6 col-md-offset-3">
										<form action="" method="post">
											<input type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf('login')?>" />
												<div class="form-group has-feedback">
													<label>Username</label>
													<div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
													<input type="text" name="username" class="form-control" placeholder="Username" value="<?=isset($_POST['username'])?$_POST['username']:''?>">
												</div>
												<div class="form-group">
													<label>Password</label>
													<div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-lock"></i>
                            </div>
														<input type="password" name="password" class="form-control" placeholder="Password" value="<?=isset($_POST['password'])?$_POST['password']:''?>">
													</div>
												</div>
												<div class="row">
													<div class="col-xs-8"> &nbsp;
													</div>
													<!-- /.col -->
													<div class="col-xs-4">
														<button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Login</button>
													</div>
													<!-- /.col -->
												</div>
										</form>
										<div style="clear:both;height:50px">&nbsp;</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/js/bootstrap.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery.steps.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/main.js"></script>
	</body>
</html>
