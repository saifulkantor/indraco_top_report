<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$namagroup= 'SUNCITY GROUP';
$slogangroup= 'Slogan';
$linktopreport='http://192.9.222.9:8080/Reports/Pages/Report.aspx?ItemPath=%2fsungroup%2fTop_report_tsh_scp';
$menupengaturan = array(
	'umum'=>array('nama'=>'Umum','icon'=>'fa fa-cogs','bgcolor'=>'#d8dbec','color'=>'black'),
	'perusahaan'=>array('nama'=>'Perusahaan','icon'=>'fa fa-building','bgcolor'=>'#c5ceff','color'=>'black'),
	'menu'=>array('nama'=>'Menu','icon'=>'fa fa-table','bgcolor'=>'#8cccff','color'=>'black'),
	'user'=>array('nama'=>'Pengguna','icon'=>'fa fa-user','bgcolor'=>'#99a9ff','color'=>'black'),
);
$data['menupengaturan']=$menupengaturan;
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Pengaturan</title>
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/bootstrap-select.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/opensans-font.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/colorlib/css/style.css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/plugins/webfont/css/all.min.css" />
		<style type="text/css">
		html,body{
			height: 100%;
		}
		.page-content{
		    min-height: 100%;
		}
		.form-register .content{
			width: 100%;
			float:none;
			background: none;
		}
		.menu {
		    padding: 5px;
		}
		.menu div {
	    display: block;
	    padding: 15px;
	    cursor: pointer;
		}
		.wizard-header{
			box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -o-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -ms-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -moz-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    -webkit-box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.15);
	    margin-bottom: 20px;
			padding-top: 10px!important;
			background-color: #d8dbec;
    	color: black;
		}
		.btnback{
			position: absolute;
			font-size: 50px;
			color: white;
			cursor:pointer;
		}
		#halamanaplikasi{
			position: absolute;
			height: 100%;
	    width: 100%;
		}
		#halamanaplikasi iframe{
			margin-top:36px;
			width: 100%;
			height: calc(100% - 36px);
		}
		.breadcrumb{
			margin: 0;
			position: absolute;
			width: 100%;
			top:0px;
			left:0px;
			height:36px;
			cursor:pointer;
			border-radius:0px;
			border-bottom:1px solid #f5f5f5;
			background: white;
			color:black;
		}
		.animation {
		    position: fixed;
		    z-index: 999;
		    -webkit-animation: ease-out normal forwards;
		    -webkit-animation-name: run;
		    -webkit-animation-duration: 0.5s;
				/* font-size: 8px; */
		}
		.animation h3 {
		    /* font-size: 10px; */
		}
		@-webkit-keyframes run {
		    0% { left:25%;width: 50%; top:20px;}
		    100%{ left:0;width:100%;top:-20px}
		}
		.pagination a, .pagination strong {
		    padding: 5px 10px;
		    background-color: #e8e8e8;
		    margin: 0px 2px;
		}
		.pilihwarna {
		    width: 30px;
		    height: 30px;
		    padding: 0;
		    cursor: pointer;
		}
		#lokasi_contoh, #menu_contoh{
			vertical-align: super;
    	padding: 5px;
		}
		</style>
	</head>

	<body>
		<script type="text/javascript">
		var id_perusahaan=0;
		function frameloaded() {
			if (id_perusahaan!=0) window.frames[0].document.querySelector('.quick-link-content a:nth-child(6)').click();
		}
		</script>
		<div id="pilihmenu" class="page-content">
			<div class="form-v1-content" style="margin-top:20px">
				<div class="wizard-form">
					<div class="form-register">
						<div class="content inner">
							<div class="wizard-header">
								<div id="btnback" class="btnback" onclick="btnkembali()"><i class="fa fa-angle-double-left"></i></div>
								<h3 class="heading"> <?=isset($menupengaturan[$halaman])?'<i class="'.$menupengaturan[$halaman]['icon'].'"></i> '.$menupengaturan[$halaman]['nama']:'<i class="fa fa-cogs"></i> Pengaturan';?></h3>
							</div>

							<div id="menupengaturan">
								<div class="col-xs-12">
									<?php $this->load->view ('pengaturan/page/'.$halaman, $data); ?>
								</div>
							</div>

							<div id="menusubkerja" style="display:none">
								<div class="col-xs-12">
								</div>
							</div>

						</div>

					</div>
					<div class="col-xs-12" style="position: absolute;bottom: -40px;">
						<p style="background-color: red;color: white;padding: 5px 20px;cursor: pointer;" class="pull-right" onclick="location.href='<?php echo BASE_URL; ?>/logout'">Logout</p>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/js/bootstrap.min.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/js/bootstrap-select.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/jquery.steps.js"></script>
		<script src="<?php echo BASE_URL; ?>/assets/plugins/colorlib/js/main.js"></script>
		<script tyle="text/javascript">
		function btnkembali() {
			<?php switch ($halaman) {
				case 'home':
					$link='';
					break;
				default:
					$link='pengaturan';
					break;
			}
			echo 'location.href="'.BASE_URL.'/'.$link.'"';
			?>
		}
		</script>
	</body>
</html>
