<?php
switch ($alert) {
	case 'berhasiltambah':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Ditambah.
</div>';
		break;
	case 'berhasilhapus':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Dihapus.
</div>';
		break;
	case 'berhasil':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Diubah.
</div>';
		break;
	case 'gagal':
		echo '<div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Gagal!</strong> Terjadi Kesalahan, Silahkan Coba Lagi.
</div>';
		break;
	default:
		echo '';
		break;
}
echo '
<button type="button" id="buttontambah" class="btn btn-success" onclick="modaluser(0)"><i class="fa fa-plus"></i> &nbsp; Tambah</button>

<table class="table table-bordered table-condensed table-striped table-hover" style="margin-top:20px">
<thead>
	<tr><th>No</th><th>Nama</th><th>Username</th><th>Level<th>Action</th></tr>
<thead>
<tbody>';
$no = $this->uri->segment('3') + 1;
foreach ($user as $key => $dt) {
	echo '<tr><td>'.$no++.'</td><td><div id="textnamauser'.$dt->id_user.'">'.$dt->nama.'</div></td>
				<td><div id="textusername'.$dt->id_user.'">'.$dt->username.'</td>
				<td><div id="textlevel'.$dt->id_user.'"> '.$dt->level.'</td>
				<td><button class="btn btn-link" onclick="modaluser('.$dt->id_user.')"><i class="fa fa-edit"></i></button> <button class="btn btn-link"><i class="fa fa-trash"></i></button></td>
	</tr>';
}
echo '</tbody></table><div class="pagination pull-right">'.$this->pagination->create_links().'</div>';
?>


<div class="modal fade" id="modaluser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah user</h4>
      </div>
      <div class="modal-body">
				<form id="formuser" action="" method="post">
					<input type="hidden" id="id_user" name="id_user" value="0" />
					<div class="form-group col-xs-12 col-sm-6">
				    <label>Nama</label>
				    	<div class="input-group"><div class="input-group-addon">
				        <i class="fa fa-book"></i>
				      </div>
							<input id="nama" type="text" class="form-control" name="nama" value="" required="" placeholder=""></div>
				  </div>

					<div class="form-group col-xs-12 col-sm-6">
						<label>Level</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-user"></i>
							</div>
							<input id="level" type="text" class="form-control" name="level" value="" required="" placeholder="">
						</div>
					</div>

					<div class="form-group col-xs-12 col-sm-6">
				    <label>Username</label>
						<div class="input-group"><div class="input-group-addon">
							<i class="fa fa-user"></i>
						</div>
						<input id="username" type="text" class="form-control" name="username" value="" required="" placeholder=""></div>
				  </div>

					<div class="form-group col-xs-12 col-sm-6">
				    <label>Password</label>
						<div class="input-group"><div class="input-group-addon">
							<i class="fa fa-code"></i>
						</div>
						<input id="password" type="text" class="form-control" name="password" value=""  placeholder=""></div>
				  </div>

					<div class="col-xs-12">
						<button type="submit" class="btn btn-success" name="submit"><i class="fa fa-save"></i> &nbsp; Simpan</button>
					</div>
					<div style="clear:both"></div>
				</form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
var user = <?=json_encode($user)?>;
function hapus(id_form='form',teks=''){
	if (confirm('Apakah Kamu yakin ingin menghapus '+teks+' ?')) {
		$('#'+id_form+' .hapus').attr('name','hapus');
		$('#'+id_form+' .submit').click();
	}
}
function modalusername(){
	$('#modalusername').modal();
}
function modallevel(){
	$('#modallevel').modal();
}
function modaluser(id_user){
	var userdipilih = user.filter(function (per) { return per.id_user == id_user })[0];
	if (userdipilih){
		$('#modaluser .modal-title').html('Edit user');
		$('#modaluser #nama').val(userdipilih['nama']);
		$('#modaluser #username').val(userdipilih['username']);
		$('#modaluser #level').val(userdipilih['level']);
	} else {
		$('#modaluser .modal-title').html('Tambah user');
		$('#modaluser #nama').val('');
		$('#modaluser #username').val('');
		$('#modaluser #level').val('');
	}
	$('#modaluser #id_user').val(id_user);
	$('.selectpicker').selectpicker('refresh');
	$('#modaluser').modal();
}

</script>
