<form action="" method="post">
<?php
switch ($alert) {
	case 'berhasil':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Diubah.
</div>';
		break;
	case 'gagal':
		echo '<div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Gagal!</strong> Terjadi Kesalahan, Silahkan Coba Lagi.
</div>';
		break;
	default:
		echo '';
		break;
}
foreach ($umum as $key => $dt) {
	echo '<div class="form-group col-xs-12">
    <label>'.$dt->nama.'</label>
    <div class="input-group"><div class="input-group-addon">
        <i class="fa fa-book"></i>
      </div><input id="umum'.$dt->id.'" type="text" class="form-control" name="umum['.$dt->id.']" value="'.$dt->data.'" required=""></div>
    <!-- /.input group -->
  </div>';
}
?>
<div class="col-xs-12">
<button type="submit" class="btn btn-primary" name="submit"> <i class="fa fa-save"></i> Simpan </button>
<p style="clear:both;padding-top:30px">NB : Semua pengaturan ini hanya akan diterapkan di program utama.</p>
</div>
</form>
