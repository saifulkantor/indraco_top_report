<?php
switch ($alert) {
	case 'berhasiltambah':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Ditambah.
</div>';
		break;
	case 'berhasilhapus':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Dihapus.
</div>';
		break;
	case 'berhasil':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Diubah.
</div>';
		break;
	case 'gagal':
		echo '<div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Gagal!</strong> Terjadi Kesalahan, Silahkan Coba Lagi.
</div>';
		break;
	default:
		echo '';
		break;
}
echo '
<button type="button" id="buttontambah" class="btn btn-success" onclick="modalmenu(0)"><i class="fa fa-plus"></i> &nbsp; Tambah</button>

<table class="table table-bordered table-condensed table-striped table-hover" style="margin-top:20px">
<thead>
	<tr><th>No</th><th>Nama Menu</th><th>Warna</th><th>Icon</th><th>Link</th><th>Level</th><th>Action</th></tr>
<thead>
<tbody>';
$no = $this->uri->segment('3') + 1;
foreach ($menu as $key => $dt) {
	$submenu = ($dt->menu_induk==NULL)?'Menu Induk':'Submenu dari '.$dt->menu_induk;
	echo '<tr><td>'.$no++.'</td><td><div id="textnamamenu'.$dt->id_menu.'">'.$dt->nama_menu.'</div></td>
				<td style="background-color:'.$dt->bgcolor.';color:'.$dt->color.';vertical-align:middle;text-align:center;"><div id="textwarnamenu'.$dt->id_menu.'"> Teks </div></td>
				<td style="vertical-align:middle;text-align:center;"><div id="texticonmenu'.$dt->id_menu.'"><i class="'.$dt->icon.'"></i></td>
				<td style="max-width:120px;word-break:break-all"><div id="textlinkmenu'.$dt->id_menu.'"><a href="'.$dt->link.'" target="_blank">'.$dt->link.'</a></div></td>
				<td>'.$submenu.'</td>
				<td><button class="btn btn-link" onclick="modalmenu('.$dt->id_menu.')"><i class="fa fa-edit"></i></button> <button class="btn btn-link"><i class="fa fa-trash"></i></button></td>
	</tr>';
}
echo '</tbody></table><div class="pagination pull-right">'.$this->pagination->create_links().'</div>';
?>
<div class="col-xs-12">
<p style="clear:both;padding-top:30px">NB : Semua pengaturan ini hanya akan diterapkan di program utama.</p>
</div>
<div class="modal fade" id="modalmenu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Menu</h4>
      </div>
      <div class="modal-body">
				<form id="formmenu" action="" method="post">
					<input type="hidden" id="id_menu" name="id_menu" value="0" />
					<div class="form-group col-xs-12 col-sm-6">
				    <label>Nama Menu</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i class="fa fa-book"></i>
				      </div><input id="nama_menu" type="text" class="form-control" name="nama_menu" value="" required="" placeholder=""></div>
				  </div>

					<div class="form-group col-xs-12 col-sm-6">
				    <label>Level Menu</label>
				    <div class="input-group" style="width: 100%">
							<select id="id_parent" name="id_parent" class="form-control">
								<option value="">-- Menu Induk --</option>
								<?php foreach ($menuall as $key => $value) {
									echo '<option value="'.$value->id_menu.'">Submenu dari '.$value->nama_menu.'</option>';
								} ?>
							</select>
						</div>
				  </div>

					<div class="col-xs-6">
				    <label>Warna</label>
				    <div class="input-group">
							<input id="menu_bgcolor" type="color" class="pilihwarna" name="bgcolor" value="#000" onchange="$('#menu_contoh').attr('style','background-color:'+$(this).val()+';color:'+$('#menu_color').val())" />
							<input id="menu_color" type="color" class="pilihwarna" name="color" value="#000" onchange="$('#menu_contoh').attr('style','background-color:'+$('#menu_bgcolor').val()+';color:'+$(this).val())" />
							<span id="menu_contoh" style="padding:6px;vertical-align:super;"> Contoh </span>
						</div>
				  </div>

					<div class="col-xs-6">
				    <label>Icon</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i id="ijenis_icon" class="fa fa-building"></i>
				      </div><input id="jenis_icon" type="text" class="form-control" name="icon" value="" required="" placeholder="fa fa-building" onkeyup="$('#ijenis_icon').attr('class',$(this).val());"></div>
				  </div>

					<div class="form-group col-xs-12">
				    <label>Link</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i class="fa fa-globe"></i>
				      </div><input id="link" type="text" class="form-control" name="link" value="" required="" placeholder="https://"></div>
				  </div>
					<div class="col-xs-12">
						<button type="submit" class="btn btn-success" name="submit"><i class="fa fa-save"></i> &nbsp; Simpan</button>
					</div>
					<div style="clear:both"></div>
				</form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">
var menu = <?=json_encode($menu)?>;
function hapus(id_form='form',teks=''){
	if (confirm('Apakah Kamu yakin ingin menghapus '+teks+' ?')) {
		$('#'+id_form+' .hapus').attr('name','hapus');
		$('#'+id_form+' .submit').click();
	}
}

function modalmenu(id_menu){
	var menudipilih = menu.filter(function (per) { return per.id_menu == id_menu })[0];
	if (menudipilih){
		$('#modalmenu .modal-title').html('Edit Menu'+menudipilih['id_parent']);
		$('#modalmenu #nama_menu').val(menudipilih['nama_menu']);
		$('#modalmenu #menu_bgcolor').val(menudipilih['bgcolor']).trigger('change');
		$('#modalmenu #menu_color').val(menudipilih['color']).trigger('change');
		$('#modalmenu #jenis_icon').val(menudipilih['icon']).trigger('keyup');
		$('#modalmenu #id_parent').val(menudipilih['id_parent']);
		$('#modalmenu #link').val(menudipilih['link']);
	} else {
		$('#modalmenu .modal-title').html('Tambah Menu');
		$('#modalmenu #menu_bgcolor').val('#000').trigger('change');
		$('#modalmenu #menu_color').val('#000').trigger('change');
		$('#modalmenu #jenis_icon').val('fa fa-building').trigger('keyup');
		$('#modalmenu #nama_menu').val('');
		$('#modalmenu #id_parent').val('NULL');
		$('#modalmenu #link').val('');
	}
	$('#modalmenu #id_menu').val(id_menu);
	$('.selectpicker').selectpicker('refresh');
	$('#modalmenu').modal();
}
function jenis_ganti(){
	var jenis_id = $('#jenis_id').val();
	var jenisdipilih = jenis_menu.filter(function (jenis) { return jenis.id_jenis_menu == jenis_id })[0];
	if (jenisdipilih){
		$('#jenis_icon').val(jenisdipilih['icon']).trigger('keyup');
		$('#jenis_jenis').val(jenisdipilih['jenis_menu']);
		$('#formjenismenu .hapus').show();
	} else {
		$('#jenis_icon').val('');
		$('#jenis_jenis').val('');
		$('#formjenismenu .hapus').hide();
	}
}
function lokasi_ganti(){
	var lokasi_id = $('#lokasi_id').val();
	var lokasidipilih = lokasi_menu.filter(function (lokasi) { return lokasi.id_lokasi_menu == lokasi_id })[0];
	if (lokasidipilih){
		$('#menu_bgcolor').val(lokasidipilih['bgcolor']).trigger('change');
		$('#menu_color').val(lokasidipilih['color']).trigger('change');
		$('#lokasi_lokasi').val(lokasidipilih['lokasi_menu']);
		$('#formlokasimenu .hapus').show();
	} else {
		$('#menu_bgcolor').val('');
		$('#menu_color').val('');
		$('#lokasi_lokasi').val('');
		$('#formlokasimenu .hapus').hide();
	}
}
</script>
