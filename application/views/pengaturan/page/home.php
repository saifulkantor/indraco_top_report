<?php
foreach ($menupengaturan as $key => $menu) {
	echo '<div class="col-xs-12 col-sm-6 col-md-3 menu menuperusahaan" style="text-align: center;">
		<div onclick="location.href=\''.BASE_URL.'/pengaturan/'.$key.'\'" style="background-color:'.$menu['bgcolor'].';color:'.$menu['color'].';">
			<i class="'.$menu['icon'].' fa-3x"></i>
			<h4>'.$menu['nama'].'</h4>
		</div>
	</div>';
}
?>
<p style="clear:both;padding-top:30px">NB : Semua pengaturan ini hanya akan diterapkan di program utama.</p>
