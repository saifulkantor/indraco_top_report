<?php
switch ($alert) {
	case 'berhasiltambah':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Ditambah.
</div>';
		break;
	case 'berhasilhapus':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Dihapus.
</div>';
		break;
	case 'berhasil':
		echo '<div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Data Berhasil Diubah.
</div>';
		break;
	case 'gagal':
		echo '<div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Gagal!</strong> Terjadi Kesalahan, Silahkan Coba Lagi.
</div>';
		break;
	default:
		echo '';
		break;
}
echo '
<button type="button" id="buttontambah" class="btn btn-success" onclick="modalperusahaan(0)"><i class="fa fa-plus"></i> &nbsp; Tambah</button>

<table class="table table-bordered table-condensed table-striped table-hover" style="margin-top:20px">
<thead>
	<tr><th>No</th><th>Nama Perusahaan</th><th>Jenis <i class="fa fa-edit" style="color:#337ab7;cursor:pointer" onclick="modaljenisperusahaan()"></i></th><th>Lokasi <i class="fa fa-edit" style="color:#337ab7;cursor:pointer" onclick="modallokasiperusahaan()"></i></th><th>Link Report</th><th>Action</th></tr>
<thead>
<tbody>';
$no = $this->uri->segment('3') + 1;
foreach ($perusahaan as $key => $dt) {
	echo '<tr><td>'.$no++.'</td><td><div id="textnamaperusahaan'.$dt->id_perusahaan.'">'.$dt->nama_perusahaan.'</div></td>
				<td><div id="textjenisperusahaan'.$dt->id_perusahaan.'"><i class="'.$dt->icon.'"></i> '.$dt->jenis_perusahaan.'</div></td>
				<td><div id="textlokasiperusahaan'.$dt->id_perusahaan.'"><div style="float:left;margin-right: 5px;height:20px;width:20px;background-color:'.$dt->bgcolor.'">&nbsp;</div> '.$dt->lokasi_perusahaan.'</div></td>
				<td><div id="textlinkperusahaan'.$dt->id_perusahaan.'"><a href="'.$dt->link_report.'" target="_blank">'.$dt->link_report.'</a></div></td>
				<td><button class="btn btn-link" onclick="modalperusahaan('.$dt->id_perusahaan.')"><i class="fa fa-edit"></i></button> <button class="btn btn-link"><i class="fa fa-trash"></i></button></td>
	</tr>';
}
echo '</tbody></table><div class="pagination pull-right">'.$this->pagination->create_links().'</div>';
?>
<div class="col-xs-12">
<p style="clear:both;padding-top:30px">NB : Semua pengaturan ini hanya akan diterapkan di program utama.</p>
</div>
<div class="modal fade" id="modalperusahaan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Perusahaan</h4>
      </div>
      <div class="modal-body">
				<form id="formperusahaan" action="" method="post">
					<input type="hidden" id="id_perusahaan" name="id_perusahaan" value="0" />
					<div class="form-group col-xs-12 col-sm-6 col-sm-4">
				    <label>Nama Perusahaan</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i class="fa fa-book"></i>
				      </div><input id="nama_perusahaan" type="text" class="form-control" name="nama_perusahaan" value="" required="" placeholder=""></div>
				  </div>

					<div class="form-group col-xs-12 col-sm-6 col-sm-4">
				    <label>Jenis Perusahaan</label>
						<select id="id_jenis_perusahaan" name="id_jenis_perusahaan" class="form-control selectpicker" data-show-subtext="true">
							<?php foreach ($jenis_perusahaan as $key => $val) {
								echo '<option value="'.$val->id_jenis_perusahaan.'" data-icon="'.$val->icon.'">'.$val->jenis_perusahaan.'</option>';
							} ?>
						</select>
				  </div>

					<div class="form-group col-xs-12 col-sm-6 col-sm-4">
						<label>Lokasi Perusahaan</label>
						<select id="id_lokasi_perusahaan" name="id_lokasi_perusahaan" class="form-control selectpicker" data-show-subtext="true">
							<?php foreach ($lokasi_perusahaan as $key => $val) {
								echo '<option value="'.$val->id_lokasi_perusahaan.'" data-content="<div style=\'float:left;height:20px;width:20px;background-color:'.$val->bgcolor.'\'>&nbsp;</div> &nbsp; '.$val->lokasi_perusahaan.'"> '.$val->lokasi_perusahaan.'</option>';
							} ?>
						</select>
					</div>

					<div class="form-group col-xs-12">
				    <label>Link Report</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i class="fa fa-globe"></i>
				      </div><input id="link_report" type="text" class="form-control" name="link_report" value="" required="" placeholder="https://"></div>
				  </div>
					<div class="col-xs-12">
						<button type="submit" class="btn btn-success" name="submit"><i class="fa fa-save"></i> &nbsp; Simpan</button>
					</div>
					<div style="clear:both"></div>
				</form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modaljenisperusahaan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Jenis Perusahaan</h4>
      </div>
      <div class="modal-body">
				<form id="formjenisperusahaan" action="" method="post">
					<input type="hidden" class="form-control hapus" value="">
					<div style="clear:both;height:20px;">&nbsp;</div>
					<div class="col-xs-12 col-sm-4">
						<label>Opsi</label>
						<select id="jenis_id" name="id_jenis_perusahaan" class="form-control selectpicker" data-show-subtext="true" onchange="jenis_ganti()">
							<option value="0" data-icon="fa fa-plus text-primary"><span class="text-primary">Tambah Baru</span></option>
							<?php foreach ($jenis_perusahaan as $key => $val) {
								echo '<option value="'.$val->id_jenis_perusahaan.'" data-icon="'.$val->icon.'">'.$val->jenis_perusahaan.'</option>';
							} ?>
						</select>
					</div>
					<div class="col-xs-6 col-sm-4">
				    <label>Icon</label>
				    <div class="input-group"><div class="input-group-addon">
				        <i id="ijenis_icon" class="fa fa-building"></i>
				      </div><input id="jenis_icon" type="text" class="form-control" name="icon" value="" required="" placeholder="fa fa-building" onkeyup="$('#ijenis_icon').attr('class',$(this).val());"></div>
				  </div>
					<div class="col-xs-6 col-sm-4">
				    <label>Jenis</label>
				    <input id="jenis_jenis" type="text" class="form-control" name="jenis_perusahaan" value="" required="" placeholder="Hotel, Mall, Waterpark, dll">
				  </div>
					<p class="btn btn-link hapus" style="display:none" onclick="hapus('formjenisperusahaan',$('#formjenisperusahaan .filter-option').text())"><i class="fa fa-trash"></i> &nbsp; Hapus</p>
					<div style="clear:both;height:20px;">&nbsp;</div>
					<div class="col-xs-12">
						<button type="submit" class="btn btn-success submit" name="submit"><i class="fa fa-save"></i> &nbsp; Simpan</button>
					</div>
					<div style="clear:both"></div>
				</form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modallokasiperusahaan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Lokasi Perusahaan</h4>
      </div>
      <div class="modal-body">
				<form id="formlokasiperusahaan" action="" method="post">
					<input type="hidden" class="form-control hapus" value="">
					<div style="clear:both;height:20px;">&nbsp;</div>
					<div class="col-xs-12 col-sm-4">
						<label>Opsi</label>
						<select id="lokasi_id" name="id_lokasi_perusahaan" class="form-control selectpicker" data-show-subtext="true" onchange="lokasi_ganti()">
							<option value="0" data-icon="fa fa-plus text-primary"><span class="text-primary">Tambah Baru</span></option>
							<?php foreach ($lokasi_perusahaan as $key => $val) {
								echo '<option value="'.$val->id_lokasi_perusahaan.'" data-content="<div style=\'float:left;height:20px;width:20px;background-color:'.$val->bgcolor.'\'>&nbsp;</div> &nbsp; '.$val->lokasi_perusahaan.'">'.$val->lokasi_perusahaan.'</option>';
							} ?>
						</select>
					</div>
					<div class="col-xs-6 col-sm-4">
				    <label>Warna</label>
				    <div class="input-group">
							<input id="lokasi_bgcolor" type="color" class="pilihwarna" name="bgcolor" value="#000" onchange="$('#lokasi_contoh').attr('style','background-color:'+$(this).val()+';color:'+$('#lokasi_color').val())" />
							<input id="lokasi_color" type="color" class="pilihwarna" name="color" value="#000" onchange="$('#lokasi_contoh').attr('style','background-color:'+$('#lokasi_bgcolor').val()+';color:'+$(this).val())" />
							<span id="lokasi_contoh" style="padding:6px;vertical-align:super;"> Contoh </span>
						</div>
				  </div>
					<div class="col-xs-6 col-sm-4">
				    <label>Nama Kota</label>
				    <input id="lokasi_lokasi" type="text" class="form-control" name="lokasi_perusahaan" value="" required="" placeholder="Sidoarjo, Surabaya, dll">
				  </div>
					<p class="btn btn-link hapus" style="display:none" onclick="hapus('formlokasiperusahaan',$('#formlokasiperusahaan .filter-option').text())"><i class="fa fa-trash"></i> &nbsp; Hapus</p>
					<div style="clear:both;height:20px;">&nbsp;</div>
					<div class="col-xs-12">
						<button type="submit" class="btn btn-success submit" name="submit"><i class="fa fa-save"></i> &nbsp; Simpan</button>
					</div>
					<div style="clear:both"></div>
				</form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
var perusahaan = <?=json_encode($perusahaan)?>;
var jenis_perusahaan = <?=json_encode($jenis_perusahaan)?>;
var lokasi_perusahaan = <?=json_encode($lokasi_perusahaan)?>;
function hapus(id_form='form',teks=''){
	if (confirm('Apakah Kamu yakin ingin menghapus '+teks+' ?')) {
		$('#'+id_form+' .hapus').attr('name','hapus');
		$('#'+id_form+' .submit').click();
	}
}
function modaljenisperusahaan(){
	$('#modaljenisperusahaan').modal();
}
function modallokasiperusahaan(){
	$('#modallokasiperusahaan').modal();
}
function modalperusahaan(id_perusahaan){
	var perdipilih = perusahaan.filter(function (per) { return per.id_perusahaan == id_perusahaan })[0];
	if (perdipilih){
		$('#modalperusahaan .modal-title').html('Edit Perusahaan');
		$('#modalperusahaan #nama_perusahaan').val(perdipilih['nama_perusahaan']);
		$('#modalperusahaan #id_jenis_perusahaan').val(perdipilih['id_jenis_perusahaan']);
		$('#modalperusahaan #id_lokasi_perusahaan').val(perdipilih['id_lokasi_perusahaan']);
		$('#modalperusahaan #link_report').val(perdipilih['link_report']);
	} else {
		$('#modalperusahaan .modal-title').html('Tambah Perusahaan');
		$('#modalperusahaan #nama_perusahaan').val('');
		$('#modalperusahaan #link_report').val('');
	}
	$('#modalperusahaan #id_perusahaan').val(id_perusahaan);
	$('.selectpicker').selectpicker('refresh');
	$('#modalperusahaan').modal();
}
function jenis_ganti(){
	var jenis_id = $('#jenis_id').val();
	var jenisdipilih = jenis_perusahaan.filter(function (jenis) { return jenis.id_jenis_perusahaan == jenis_id })[0];
	if (jenisdipilih){
		$('#jenis_icon').val(jenisdipilih['icon']).trigger('keyup');
		$('#jenis_jenis').val(jenisdipilih['jenis_perusahaan']);
		$('#formjenisperusahaan .hapus').show();
	} else {
		$('#jenis_icon').val('');
		$('#jenis_jenis').val('');
		$('#formjenisperusahaan .hapus').hide();
	}
}
function lokasi_ganti(){
	var lokasi_id = $('#lokasi_id').val();
	var lokasidipilih = lokasi_perusahaan.filter(function (lokasi) { return lokasi.id_lokasi_perusahaan == lokasi_id })[0];
	if (lokasidipilih){
		$('#lokasi_bgcolor').val(lokasidipilih['bgcolor']).trigger('change');
		$('#lokasi_color').val(lokasidipilih['color']).trigger('change');
		$('#lokasi_lokasi').val(lokasidipilih['lokasi_perusahaan']);
		$('#formlokasiperusahaan .hapus').show();
	} else {
		$('#lokasi_bgcolor').val('');
		$('#lokasi_color').val('');
		$('#lokasi_lokasi').val('');
		$('#formlokasiperusahaan .hapus').hide();
	}
}
</script>
