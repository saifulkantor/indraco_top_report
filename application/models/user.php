<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Model {
	private $table = 'user';
	private $id = 'id_user';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->row();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}

	public function login($username='',$password='') {
		$datauser = $this->getWhere(['username'=>$username,'password'=>$this->keamanan->generatepassword($password),]);
		if ($datauser) {
			$datauser=$datauser[0];
			$datauser->auth_key=$this->keamanan->generateauth_key($datauser->id_user);
			$this->updateone(['auth_key'=>$datauser->auth_key],['id_user'=>$datauser->id_user]);
		}
		return $datauser;
	}

	public function getWhere($filters){
		$sql = "select *
				from {$this->table} ";
		if ($filters!=null) {
      $sql .= ' WHERE ';
      foreach ($filters as $key => $filter) {
        $key = ($key=='id_kurir')?'id':$key;
        $sql .= ' '.$key.'="'.$filter.'" AND';
      }
      $sql=substr($sql, 0, -3);
    }
		$sql = $this->db->query($sql);
		return $sql->result();
	}
	public function getAll(){
		$sql = "select a.*
				from {$this->table} a";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getData($number,$offset){
		$sql = "select a.*
				from {$this->table} a
				LIMIT ".$number." OFFSET ".$offset;
		$query = $this->db->query($sql);
		return $query->result();
		// return $query = $this->db->get($this->table,$number,$offset)->result();
	}

	// function getData($number,$offset){
	// 	$sql = "select a.*,c.nama_menu
	// 			c.lokasi_perusahaan
	// 			from {$this->table} a
	// 			left join user_menuauth b on a.{$this->id} = b.id_user
	// 			left join menu c on b.id_menu = c.id_menu
	// 			left join user_perusahaan d on a.{$this->id} = b.id_user
	// 			left join perusahaan e on b.id_menu = c.id_menu
	// 			left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
	// 			LIMIT ".$number." OFFSET ".$offset;
	// 	$query = $this->db->query($sql);
	// 	return $query->result();
	// 	// return $query = $this->db->get($this->table,$number,$offset)->result();
	// }

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				if ($key=='password') {
						if ($data!='') {
							$data=$this->keamanan->generatepassword($data);
							$query .= ' '.$key.'="'.$data.'",';
						}
				} else {
					$query .= ' '.$key.'="'.$data.'",';
				}
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

	function createone($datas=null) {
		if ($datas!='') {
			$query = '(';
			$val = '(';
			foreach ($datas as $key => $data) {
				if ($key!='id_user') {
					if ($key=='password') {
						if ($data!='') $data=$datas['username'];
						$data=$this->keamanan->generatepassword($data);
					}
					$val .= $key.',';
					$query .= ($data==null)?'NULL,':'"'.$data.'",';
				}
			}
			$val .= 'auth_key,';
			$query .= '"--",';
			$val=substr($val, 0, -1); $val.=')';
			$query=substr($query, 0, -1); $query.=');';
			$query = 'INSERT INTO '.$this->table.' '.$val.' VALUES '.$query;
			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		}
	}

}
