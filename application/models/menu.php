<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class menu extends CI_Model {
	private $table = 'menu';
	private $id = 'id_menu';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id=0){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		return $query->row();
	}

	public function getAll(){
		$sql = "select a.*
				from {$this->table} a";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getMenu($id=0,$id_parent=null,$id_user=0){
		$id_parent=($id_parent!=null)?' = '.$id_parent:' IS NULL';
		$sql = "select a.*, count(b.id_menu) AS anak
				from {$this->table} a
				left join menu b on a.id_menu = b.id_parent OR b.id_menu IS NULL
				where a.{$this->id} = ".$id." AND a.id_parent ".$id_parent." GROUP BY a.id_menu order by a.id_menu";
		$query = $this->db->query($sql, [$id,$id_parent]);
		return $query->result();
	}

	function getData($number,$offset){
		$sql = "select a.*, max(b.nama_menu) AS menu_induk
				from {$this->table} a
				left join menu b on a.id_parent = b.id_menu OR b.id_menu IS NULL
				where 1 GROUP BY a.id_menu order by a.id_menu
				LIMIT ".$number." OFFSET ".$offset;
		$query = $this->db->query($sql);
		return $query->result();
		// return $query = $this->db->get($this->table,$number,$offset)->result();
	}

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				if ($key!='id_menu') $query .= ($data==null || $data=='')?' '.$key.'=NULL,':' '.$key.'="'.$data.'",';
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

	function createone($datas=null) {
		if ($datas!='') {
			$query = '(';
			$val = '(';
			foreach ($datas as $key => $data) {
				if ($key!='id_menu') {
					$val .= $key.',';
					$query .= ($data==null)?'NULL,':'"'.$data.'",';
				}
			}
			$val=substr($val, 0, -1); $val.=')';
			$query=substr($query, 0, -1); $query.=');';
			$query = 'INSERT INTO '.$this->table.' '.$val.' VALUES '.$query;
			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		}
	}

}
