<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perusahaan extends CI_Model {
	private $table = 'perusahaan';
	private $id = 'id_perusahaan';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id=0){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		return $query->row();
	}

	public function getAll(){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				order by a.urutan";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getMilikku($id_user=0) {
		if ($id_user==1) return $this->getAll();
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				left join user_perusahaan d on a.id_perusahaan=d.id_perusahaan
				where d.id_user=".$id_user;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function getMenu($id=0,$id_parent=null,$id_user=0){
		$id_parent=($id_parent!=null)?' = '.$id_parent:' IS NULL';
		$sql = "select a.nama_perusahaan, b.selesai, b.id_perusahaan_aplikasi, c.*, d.keyword, count(e.id_menu) AS anak
				from {$this->table} a
				left join perusahaan_menu b on a.id_perusahaan = b.id_perusahaan
				left join menu c on b.id_menu = c.id_menu
				left join user_menuauth d on b.id_menu = d.id_menu AND (d.id_user=".$id_user." OR id_user IS NULL)
				left join menu e on c.id_menu = e.id_parent OR e.id_menu IS NULL
				where a.{$this->id} = ".$id." AND c.id_parent ".$id_parent." GROUP BY c.id_menu order by c.id_menu";
		$query = $this->db->query($sql, [$id,$id_parent]);
		return $query->result();
	}

	function getData($number,$offset){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				LIMIT ".$number." OFFSET ".$offset;
		$query = $this->db->query($sql);
		return $query->result();
		// return $query = $this->db->get($this->table,$number,$offset)->result();
	}

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				if ($key!='id_perusahaan') $query .= ' '.$key.'="'.$data.'",';
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

	function createone($datas=null) {
		if ($datas!='') {
			$query = '(';
			$val = '(';
			foreach ($datas as $key => $data) {
				if ($key!='id_perusahaan') {
					$val .= $key.',';
					$query .= ($data==null)?'NULL,':'"'.$data.'",';
				}
			}
			$val=substr($val, 0, -1); $val.=')';
			$query=substr($query, 0, -1); $query.=');';
			$query = 'INSERT INTO '.$this->table.' '.$val.' VALUES '.$query;
			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		}
	}

}
