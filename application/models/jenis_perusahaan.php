<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jenis_perusahaan extends CI_Model {
	private $table = 'jenis_perusahaan';
	private $id = 'id_jenis_perusahaan';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		return $query->row();
	}

	public function getAll(){
		$sql = "select a.*
				from {$this->table} a";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				if ($key!='id_jenis_perusahaan') $query .= ' '.$key.'="'.$data.'",';
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

	function createone($datas=null) {
		if ($datas!='') {
			$query = '(';
			$val = '(';
			foreach ($datas as $key => $data) {
				if ($key!='id_jenis_perusahaan') {
					$val .= $key.',';
					$query .= ($data==null)?'NULL,':'"'.$data.'",';
				}
			}
			$val=substr($val, 0, -1); $val.=')';
			$query=substr($query, 0, -1); $query.=');';
			$query = 'INSERT INTO '.$this->table.' '.$val.' VALUES '.$query;
			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		}
	}

	function deleteone($wheres=null) {
		if ($wheres!=null) {
			$query = 'DELETE FROM '.$this->table;
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

}
