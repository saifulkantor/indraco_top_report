<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends MY_Model{
	private $table = 'm_menu';
	private $id = 'id_barang';
	private $kode = 'kode_barang';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id){
		$sql = "select a.*, b.kode_perkiraan as kode_perkiraan_persediaan, b.nama_perkiraan as nama_perkiraan_persediaan,
					   c.kode_perkiraan as kode_perkiraan_jual, c.nama_perkiraan as nama_perkiraan_jual,
					   d.kode_perkiraan as kode_perkiraan_jual, d.nama_perkiraan as nama_perkiraan_jual
				from {$this->table} a 
				left join m_perkiraan b on a.id_perkiraan_persediaan = b.id_perkiraan
				left join m_perkiraan c on a.id_perkiraan_hpp = c.id_perkiraan
				left join m_perkiraan d on a.id_perkiraan_jual = d.id_perkiraan
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->row();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}

	public function getAll(){
		$sql = "select a.*
				from {$this->table} a 
				inner join m_menu_header b on a.modul = b.nama
                order by b.urutan, a.urutan";
		$query = $this->db->query($sql);
		return $query->result();
	}

}