<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class lokasi_perusahaan extends CI_Model {
	private $table = 'lokasi_perusahaan';
	private $id = 'id_lokasi_perusahaan';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->row();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}

	public function getAll(){
		$sql = "select a.*
				from {$this->table} a";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				$query .= ' '.$key.'="'.$data.'",';
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

}
