<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('id_user')){
			$this->load->model('perusahaan');
			$this->load->model('jenis_perusahaan');
			$this->load->model('lokasi_perusahaan');
			$this->load->model('umum');

			$this->load->view('page/home',[
				'perusahaan'=>$this->perusahaan,
				'jenis_perusahaan'=>$this->jenis_perusahaan,
				'lokasi_perusahaan'=>$this->lokasi_perusahaan,
				'linkTopReport'=>$this->umum->getTopReport(),
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function login()
	{
		if($this->session->userdata('id_user')){
			redirect('/', 'refresh');
		} else {
			$this->load->model('user');
			$error='';
			if (isset($_POST['login'])&&isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['_csrf'])) {
				if ($_POST['_csrf']==$this->keamanan->generatecsrf('login')){
					if ($datauser=$this->user->login($_POST['username'],$_POST['password'])) {
						$newdata = array(
						        'id_user'  => $datauser->id_user,
						        'nama'     =>  $datauser->nama,
										'auth_key'     =>  $datauser->auth_key,
						        'level' => ($datauser->level!=null)?$datauser->level:'user',
										'nama_perusahaan' => ''
						);
						$this->session->set_userdata($newdata);
						redirect('/', 'refresh');
					} else {
						$error='Username/Password Salah. Silahkan Coba Lagi';
					}
				} else {
					$error='Sesi telah habis, silahkan login kembali.';
				}
				$this->load->view('/login',['error'=>$error]);
			} else {
				$this->load->view('/login');
			}
		}
	}

	public function logout()
	{
		session_destroy();
		redirect('', 'refresh');
	}

}
