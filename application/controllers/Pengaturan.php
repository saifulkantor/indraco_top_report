<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('id_user')){
			$this->load->view('pengaturan/layout',[
				'halaman'=>'home',
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function perusahaan()
	{
		if($this->session->userdata('id_user')){
			$this->load->model('perusahaan');
			$this->load->model('jenis_perusahaan');
			$this->load->model('lokasi_perusahaan');
			$data['alert']='';
			if (isset($_POST['submit'])) {
				unset($_POST['submit']);
				if (isset($_POST['id_perusahaan'])) {
					if ($this->perusahaan->get($_POST['id_perusahaan'])!=null){
						$this->perusahaan->updateone($_POST,['id_perusahaan'=>$_POST['id_perusahaan']]);
						$data['alert']='berhasil';
					} else {
						$this->perusahaan->createone($_POST);
						$data['alert']='berhasiltambah';
					}
				} else if (isset($_POST['id_jenis_perusahaan'])) {
					if ($this->jenis_perusahaan->get($_POST['id_jenis_perusahaan'])!=null){
						if (isset($_POST['hapus'])) {
							$this->jenis_perusahaan->deleteone(['id_jenis_perusahaan'=>$_POST['id_jenis_perusahaan']]);
							$data['alert']='berhasilhapus';
						} else {
							$this->jenis_perusahaan->updateone($_POST,['id_jenis_perusahaan'=>$_POST['id_jenis_perusahaan']]);
							$data['alert']='berhasil';
						}
					} else {
						if (!isset($_POST['hapus'])) {
							$this->jenis_perusahaan->createone($_POST);
							$data['alert']='berhasiltambah';
						}
					}
				} else if (isset($_POST['id_lokasi_perusahaan'])) {
					$data['alert']='berhasil';
				} else {
					$data['alert']='gagal';
				}
		  }
			$data['perusahaan']=$this->perusahaan->getMilikku($this->session->userdata('id_user'));
			$data['jenis_perusahaan']=$this->jenis_perusahaan->getAll();
			$data['lokasi_perusahaan']=$this->lokasi_perusahaan->getAll();

			$jumlah_data = count($data['perusahaan']);
			$this->load->library('pagination');
			$config['base_url'] = BASE_URL.'/pengaturan/perusahaan';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 10;
			$from = $this->uri->segment(3)??0;
			$this->pagination->initialize($config);
			$data['perusahaan'] = $this->perusahaan->getData($config['per_page'],$from);

			$this->load->view('pengaturan/layout',[
				'halaman'=>'perusahaan',
				'data'=>$data,
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function menu()
	{
		if($this->session->userdata('id_user')){
			$this->load->model('menu');
			$data['alert']='';
			if (isset($_POST['submit'])) {
				unset($_POST['submit']);
				if (isset($_POST['id_menu'])) {
					if ($this->menu->get($_POST['id_menu'])!=null){
						$this->menu->updateone($_POST,['id_menu'=>$_POST['id_menu']]);
						$data['alert']='berhasil';
					} else {
						$this->menu->createone($_POST);
						$data['alert']='berhasiltambah';
					}
				} else {
					$data['alert']='gagal';
				}
		  }
			$data['menuall']=$this->menu->getAll();

			$jumlah_data = count($data['menuall']);
			$this->load->library('pagination');
			$config['base_url'] = BASE_URL.'/pengaturan/menu';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 10;
			$from = $this->uri->segment(3)??0;
			$this->pagination->initialize($config);
			$data['menu'] = $this->menu->getData($config['per_page'],$from);

			$this->load->view('pengaturan/layout',[
				'halaman'=>'menu',
				'data'=>$data,
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function user()
	{
		if($this->session->userdata('id_user')){
			$this->load->model('user');
			$data['alert']='';
			if (isset($_POST['submit'])) {
				unset($_POST['submit']);
				if (isset($_POST['id_user'])) {
					if ($this->user->get($_POST['id_user'])['data']!=null){
						if (isset($_POST['hapus'])) {
							// /$this->user->deleteone(['id_user'=>$_POST['id_user']]);
							//$data['alert']='berhasilhapus';
						} else {
							$this->user->updateone($_POST,['id_user'=>$_POST['id_user']]);
							$data['alert']='berhasil';
						}
					} else {
						$this->user->createone($_POST);
						$data['alert']='berhasiltambah';
					}
				} else {
					$data['alert']='gagal';
				}
		  }
			$data['user']=$this->user->getAll();

			$jumlah_data = count($data['user']);
			$this->load->library('pagination');
			$config['base_url'] = BASE_URL.'/pengaturan/user';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 10;
			$from = $this->uri->segment(3)??0;
			$this->pagination->initialize($config);
			$data['user'] = $this->user->getData($config['per_page'],$from);

			$this->load->view('pengaturan/layout',[
				'halaman'=>'user',
				'data'=>$data,
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function umum()
	{
		if($this->session->userdata('id_user')){
			$this->load->model('umum');
			$data['alert']='';
			if (isset($_POST['submit'])) {
		 	 	if (isset($_POST['umum'])) {
					foreach ($_POST['umum'] as $key => $value) {
						$this->umum->updateone(['data'=>$value],['id'=>$key]);
					}
					$data['alert']='berhasil';
				} else {
					$data['alert']='gagal';
				}
		  }
			$data['umum']=$this->umum->getAll();
			$this->load->view('pengaturan/layout',[
				'halaman'=>'umum',
				'data'=>$data,
			]);
		} else {
			redirect('/login', 'refresh');
		}
	}

}
