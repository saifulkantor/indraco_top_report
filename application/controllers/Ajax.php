<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function menu_perusahaan()
	{
    $data='';
    if (isset($_POST['id_perusahaan'])&&isset($_POST['id_parent'])&&isset($_POST['_csrf'])&&isset($_POST['id_user'])&&isset($_POST['auth_key'])) {
      $this->load->model('perusahaan');
      $data='ew';
   	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()) {
   		 $data = $this->perusahaan->getMenu($_POST['id_perusahaan'],$_POST['id_parent'],$_POST['id_user']);
   	 }
    }
    echo json_encode($data);
   }

}
