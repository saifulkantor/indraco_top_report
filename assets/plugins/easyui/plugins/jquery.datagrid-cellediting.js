(function($){
	$.extend($.fn.datagrid.defaults, {
		clickToEdit: true,
		dblclickToEdit: false,
		RowAdd: true,
		RowDelete: true,
		RowEdit: true,
		navHandler: {
			'37': function(e){
				var opts = $(this).datagrid('options');
				return navHandler.call(this, e, opts.isRtl?'right':'left');
			},
			'39': function(e){
				var opts = $(this).datagrid('options');
				return navHandler.call(this, e, opts.isRtl?'left':'right');
			},
			'38': function(e){
				return navHandler.call(this, e, 'up');
			},
			'40': function(e){
				return navHandler.call(this, e, 'down');
			},
			'13': function(e){
				return enterHandler.call(this, e);
			},
			'27': function(e){
				return escHandler.call(this, e);
			},
			'8': function(e){
				return clearHandler.call(this, e);
			},
			'46': function(e){
				var dg = $(this);
                var param = dg.datagrid('cell');
				var opts = dg.datagrid('options');
				if (e.shiftKey && opts.RowDelete) {
					if (dg.datagrid('getRows').length > 0) {
						$.messager.confirm('Confirm', 'Anda Yakin Akan Menghapus Data Pada Baris Ini ?', function (r) {
							if (r) {
								var rows = dg.datagrid('getRows');
								var row = rows[param.index];
								dg.datagrid('deleteRow', param.index);
								opts.onAfterDeleteRow.call(this, param.index, row)
							}
							var index = (dg.datagrid('getRows')).length == param.index ? ((dg.datagrid('getRows')).length - 1) : param.index;
							dg.datagrid('gotoCell', {
								index: index,
								field: param.field
							})
						})
					}
					return false
				} else if (e.shiftKey && !opts.RowDelete) {
					$.messager.alert('Warning', 'Delete detail sedang tidak diperbolehkan','warning', function(e){
						if (typeof param === 'undefined' || param === null || param === 'null') {
							var fields = dg.datagrid('getColumnFields');
							dg.datagrid('gotoCell', {index:0, field:fields[0]});
						} else {
							dg.datagrid('gotoCell', {index:param.index, field:param.field});
						}
					});
				} else {
					return clearHandler.call(this, e);
				}
			},
			'keypress': function(e){
				if (e.metaKey || e.ctrlKey){
					return;
				}
				var dg = $(this);
				var param = dg.datagrid('cell');	// current cell information
				if (!param) {
					return;
				}
				var input = dg.datagrid('input', param);
				if (!input){
					var tmp = $('<span></span>');
					tmp.html(String.fromCharCode(e.which));
					var c = tmp.text();
					tmp.remove();
					if (c){
						dg.datagrid('editCell', {
							index: param.index,
							field: param.field,
							value: c
						});
						return false;
					}
				}
			}
		},
		onBeforeCellEdit: function(index, field){},
		onCellEdit: function(index, field, value){},
		onSelectCell: function(index, field){},
		onUnselectCell: function(index, field){},
		onAfterDeleteRow: function(index, row){},
	});

	function navHandler(e, dir){
		var dg = $(this);
		var param = dg.datagrid('cell');
		var input = dg.datagrid('input', param);
		if (!input){
			dg.datagrid('gotoCell', dir);
			return false;
		}
	}

	function enterHandler(e){
		var dg = $(this);
		var cell = dg.datagrid('cell');
		if (!cell){return;}
		var input = dg.datagrid('input', cell);
		if (input){
			if (input[0].tagName.toLowerCase() == 'textarea' && e.ctrlKey == false){
				return;
			}
			endCellEdit(this, true);
		} else {
			dg.datagrid('editCell', cell);
		}
		return false;
	}

	function escHandler(e){
		endCellEdit(this, false);
		return false;
	}

	function clearHandler(e){
		var dg = $(this);
		var param = dg.datagrid('cell');
		if (!param){return;}
		var input = dg.datagrid('input', param);
		if (!input){
			dg.datagrid('editCell', {
				index: param.index,
				field: param.field,
				value: ''
			});
			return false;
		}
	}

	function getCurrCell(target){
		var cell = $(target).datagrid('getPanel').find('td.datagrid-row-selected');
		if (cell.length){
			return {
				index: parseInt(cell.closest('tr.datagrid-row').attr('datagrid-row-index')),
				field: cell.attr('field')
			};
		} else {
			return null;
		}
	}

	function unselectCell(target, p){
		var opts = $(target).datagrid('options');
		var cell = opts.finder.getTr(target, p.index).find('td[field="'+p.field+'"]');
		cell.removeClass('datagrid-row-selected');
		opts.onUnselectCell.call(target, p.index, p.field);
	}

	function unselectAllCells(target){
		var panel = $(target).datagrid('getPanel');
		panel.find('td.datagrid-row-selected').removeClass('datagrid-row-selected');
	}

	function selectCell(target, p){
		var opts = $(target).datagrid('options');
		if (opts.singleSelect){
			unselectAllCells(target);
		}
		var cell = opts.finder.getTr(target, p.index).find('td[field="'+p.field+'"]');
		cell.addClass('datagrid-row-selected');
		opts.onSelectCell.call(target, p.index, p.field);
	}

	function getSelectedCells(target){
		var cells = [];
		var panel = $(target).datagrid('getPanel');
		panel.find('td.datagrid-row-selected').each(function(){
			var td = $(this);
			cells.push({
				index: parseInt(td.closest('tr.datagrid-row').attr('datagrid-row-index')),
				field: td.attr('field')
			});
		});
		return cells;
	}

	function gotoCell(target, p){
		var dg = $(target);
		var opts = dg.datagrid('options');
		var panel = dg.datagrid('getPanel').focus();

		var cparam = dg.datagrid('cell');
		if (cparam){
			var input = dg.datagrid('input', cparam);
			if (input){
				input.focus();
				return;
			}
		}

		if (typeof p == 'object'){
			_go(p);
			return;
		}
		var cell = panel.find('td.datagrid-row-selected');
		if (!cell){return;}
		var fields = dg.datagrid('getColumnFields',true).concat(dg.datagrid('getColumnFields'));
		var field = cell.attr('field');
		var tr = cell.closest('tr.datagrid-row');
		var rowIndex = parseInt(tr.attr('datagrid-row-index'));
		var colIndex = $.inArray(field, fields);

		if (p == 'up' && rowIndex > 0){
			var rows = dg.datagrid('getRows');
            if (opts.RowDelete) {
				var data = [];
				if (isNaN(rowIndex) == false && rows.length > 0) {
					data = $.map(rows[rowIndex], function (el) {
							return el
						})
				}

				for(let i = 0, ln = fields.length; i < ln; i++) {
					var col = dg.datagrid('getColumnOption', fields[i]);
					var firstField = false;
					if (typeof col != 'undefined' && col != 'null') {
						if (typeof col.editor != 'undefined' && col.editor != 'null') {
							if (typeof col.editor.options != 'undefined' && col.editor.options != 'null') {
								if (col.editor.options.required) {
									if (rows[rowIndex][fields[i]] === '')
										dg.datagrid('deleteRow', rowIndex);
									break
								}
							}
						}
					}
				}
			}
			rowIndex--;
		} else if (p == 'down'){
			if (opts.finder.getRow(target, rowIndex+1)){
				rowIndex++;
			} else {
				var insertRow = true;
				var rows = dg.datagrid('getRows');
				var data = [];
				if (isNaN(rowIndex) == false && rows.length > 0) {
					data = $.map(rows[rowIndex], function (el) {
							return el
						})
				}
				var row = {};
				for(let i = 0, ln = fields.length; i < ln; i++) {
					var col = dg.datagrid('getColumnOption', fields[i]);
					var isi = '';
					if (typeof col != 'undefined' && col != 'null') {
						if (typeof col.editor != 'undefined' && col.editor != 'null') {
							if (col.editor.type == 'numberbox') {
								isi = 0
							} else if (col.editor.type == 'datebox') {
								isi = dateFormat()
							}
							if (typeof col.editor.options != 'undefined' && col.editor.options != 'null') {
								if (col.editor.options.required && data.length > 0) {
									if (rows[rowIndex][fields[i]] === '') {
										insertRow = false;
										$.messager.alert('Warning', 'Kolom ' + col.title + ' belum diisi...','warning', function(e){
											dg.datagrid('gotoCell', {
												index: rowIndex,
												field: fields[i]
											});
										});
										break
									}
								}
							}
						}
					}
					row[fields[i]] = isi;
				}
				if (insertRow && opts.RowAdd) {
					dg.datagrid('appendRow', row);
					var field = '';
					for(let i = 0, ln = fields.length; i < ln; i++) {
						var col = dg.datagrid('getColumnOption', fields[i]);
						if (!col.hidden) {
							field = fields[i];
							break
						}
					}
					var row = dg.datagrid('getRows');
					dg.datagrid('gotoCell', {
						index: row.length - 1,
						field: field
					});
					return
				} else if (!opts.RowAdd) {
					var cell = dg.datagrid('cell')
					$.messager.alert('Warning', 'Tambah detail sedang tidak diperbolehkan','warning', function(e){
						if (typeof cell === 'undefined' || cell === null || cell === 'null') {
							var fields = dg.datagrid('getColumnFields');
							dg.datagrid('gotoCell', {index:0, field:fields[0]});
						} else {
							dg.datagrid('gotoCell', {index:cell.index, field:cell.field});
						}
					});
				}
			}
		} else if (p == 'left'){
			var i = colIndex - 1;
			while(i >= 0){
				var col = dg.datagrid('getColumnOption', fields[i]);
				if (!col.hidden){
					colIndex = i;
					break;
				}
				i--;
			}
		} else if (p == 'right'){
			for(let i = colIndex + 1, ln = fields.length-1; i <= ln; i++) {
				var col = dg.datagrid('getColumnOption', fields[i]);
				if (!col.hidden){
					colIndex = i;
					break;
				}
			}
		}

		field = fields[colIndex];

		_go({index:rowIndex, field:field});

		function _go(p){
			dg.datagrid('scrollTo', p.index);
			unselectAllCells(target);
			selectCell(target, p);
			var td = opts.finder.getTr(target, p.index, 'body', 2).find('td[field="'+p.field+'"]');
			if (td.length){
				var body2 = dg.data('datagrid').dc.body2;
				var left = td.position().left;
				if (left < 0){
					body2._scrollLeft(body2._scrollLeft() + left*(opts.isRtl?-1:1));
				} else if (left+td._outerWidth()>body2.width()){
					body2._scrollLeft(body2._scrollLeft() + (left+td._outerWidth()-body2.width())*(opts.isRtl?-1:1));
				}
			}
		}
	}

	// end the current cell editing
	function endCellEdit(target, accepted){
		var dg = $(target);
		var cell = dg.datagrid('cell');
		if (cell){
			var input = dg.datagrid('input', cell);
			if (input){
				if (accepted){
					if (dg.datagrid('validateRow', cell.index)){
						dg.datagrid('endEdit', cell.index);
						dg.datagrid('gotoCell', cell);
					} else {
						dg.datagrid('gotoCell', cell);
						input.focus();
						return false;
					}
				} else {
					dg.datagrid('cancelEdit', cell.index);
					dg.datagrid('gotoCell', cell);
				}
			}
		}
		return true;
	}

	function editCell(target, param){
		var dg = $(target);
		var opts = dg.datagrid('options');
		var input = dg.datagrid('input', param);
		if (input){
			dg.datagrid('gotoCell', param);
			input.focus();
			return;
		}
		if (!endCellEdit(target, true)){return;}
		if (opts.onBeforeCellEdit.call(target, param.index, param.field) == false){
			return;
		}

		var fields = dg.datagrid('getColumnFields',true).concat(dg.datagrid('getColumnFields'));
		$.map(fields, function(field){
			var col = dg.datagrid('getColumnOption', field);
			col.editor1 = col.editor;
			if (field != param.field){
				col.editor = null;
			}
		});

		var col = dg.datagrid('getColumnOption', param.field);
		if (col.editor){
			if (opts.RowEdit) {
				dg.datagrid('beginEdit', param.index);
				var input = dg.datagrid('input', param);
				if (input){
					dg.datagrid('gotoCell', param);
					setTimeout(function(){
						input.unbind('.cellediting').bind('keydown.cellediting', function(e){
							if (e.keyCode == 13){
								return opts.navHandler['13'].call(target, e);
							}
						});
						
						if (param.value !== undefined) {
							input.val(param.value)
						}
						opts.onCellEdit.call(target, param.index, param.field, param.value);
					}, 0);
				} else {
					dg.datagrid('cancelEdit', param.index);
					dg.datagrid('gotoCell', param);
				}
			} else {
				$.messager.alert('Warning', 'Edit detail sedang tidak diperbolehkan','warning', function(e){
					if (typeof param === 'undefined' || param === null || param === 'null') {
						var opts = dg.datagrid('getColumnFields');
						dg.datagrid('gotoCell', {index:0, field:opts[0]});
					} else {
						dg.datagrid('gotoCell', {index:param.index, field:param.field});
					}
				});
			}
		} else {
			dg.datagrid('gotoCell', param);
		}

		$.map(fields, function(field){
			var col = dg.datagrid('getColumnOption', field);
			col.editor = col.editor1;
		});
	}

	function enableCellSelecting(target){
		var dg = $(target);
		var state = dg.data('datagrid');
		var panel = dg.datagrid('getPanel');
		var opts = state.options;
		var dc = state.dc;
		panel.attr('tabindex',1).css('outline-style','none').unbind('.cellediting').bind('keydown.cellediting', function(e){
			var h = opts.navHandler[e.keyCode];
			if (h){
				return h.call(target, e);
			}
		});
		dc.body1.add(dc.body2).unbind('.cellediting').bind('click.cellediting', function(e){
			var tr = $(e.target).closest('.datagrid-row');
			if (tr.length && tr.parent().length){
				var td = $(e.target).closest('td[field]', tr);
				if (td.length){
					var index = parseInt(tr.attr('datagrid-row-index'));
					var field = td.attr('field');
					var p = {
						index: index,
						field: field
					};
					if (opts.singleSelect){
						selectCell(target, p);
					} else {
						if (opts.ctrlSelect){
							if (e.ctrlKey){
								if (td.hasClass('datagrid-row-selected')){
									unselectCell(target, p);
								} else {
									selectCell(target, p);
								}
							} else {
								unselectAllCells(target);
								selectCell(target, p);
							}
						} else {
							if (td.hasClass('datagrid-row-selected')){
								unselectCell(target, p);
							} else {
								selectCell(target, p);
							}
						}
					}
				}
			}
		}).bind('mouseover.cellediting', function(e){
			var td = $(e.target).closest('td[field]');
			if (td.length){
				td.addClass('datagrid-row-over');
				td.closest('tr.datagrid-row').removeClass('datagrid-row-over');
			}
		}).bind('mouseout.cellediting', function(e){
			var td = $(e.target).closest('td[field]');
			td.removeClass('datagrid-row-over');
		});

		opts.isRtl = dg.datagrid('getPanel').css('direction').toLowerCase()=='rtl';
		opts.OldOnBeforeSelect = opts.onBeforeSelect;
		opts.onBeforeSelect = function(){
			return false;
		};
		dg.datagrid('clearSelections');
	}

	function disableCellSelecting(target){
		var dg = $(target);
		var state = dg.data('datagrid');
		var panel = dg.datagrid('getPanel');
		var opts = state.options;
		opts.onBeforeSelect = opts.OldOnBeforeSelect || opts.onBeforeSelect;
		panel.unbind('.cellediting').find('td.datagrid-row-selected').removeClass('datagrid-row-selected');
		var dc = state.dc;
		dc.body1.add(dc.body2).unbind('.cellediting');
	}

	function enableCellEditing(target){
		var dg = $(target);
		var opts = dg.datagrid('options');
		var panel = dg.datagrid('getPanel');
		panel.attr('tabindex',1).css('outline-style','none').unbind('.cellediting').bind('keydown.cellediting', function(e){
			var h = opts.navHandler[e.keyCode];
			if (h){
				return h.call(target, e);
			}
		}).bind('keypress.cellediting', function(e){
			return opts.navHandler['keypress'].call(target, e);
		});
		panel.panel('panel').unbind('.cellediting').bind('keydown.cellediting', function(e){
			e.stopPropagation();
		}).bind('keypress.cellediting', function(e){
			e.stopPropagation();
		}).bind('mouseover.cellediting', function(e){
			var td = $(e.target).closest('td[field]');
			if (td.length){
				td.addClass('datagrid-row-over');
				td.closest('tr.datagrid-row').removeClass('datagrid-row-over');
			}
		}).bind('mouseout.cellediting', function(e){
			var td = $(e.target).closest('td[field]');
			td.removeClass('datagrid-row-over');
		});

		opts.isRtl = dg.datagrid('getPanel').css('direction').toLowerCase()=='rtl';
		opts.oldOnClickCell = opts.onClickCell;
		opts.oldOnDblClickCell = opts.onDblClickCell;
		opts.onClickCell = function(index, field, value){
			if (opts.clickToEdit){
				$(this).datagrid('editCell', {index:index,field:field});
			} else {
				if (endCellEdit(this, true)){
					$(this).datagrid('gotoCell', {
						index: index,
						field: field
					});
				}
			}
			opts.oldOnClickCell.call(this, index, field, value);
		}
		if (opts.dblclickToEdit){
			opts.onDblClickCell = function(index, field, value){
				$(this).datagrid('editCell', {index:index,field:field});
				opts.oldOnDblClickCell.call(this, index, field, value);
			}
		}
		opts.OldOnBeforeSelect = opts.onBeforeSelect;
		opts.onBeforeSelect = function(){
			return false;
		};
		dg.datagrid('clearSelections')
	}

	function disableCellEditing(target){
		var dg = $(target);
		var panel = dg.datagrid('getPanel');
		var opts = dg.datagrid('options');
		opts.onClickCell = opts.oldOnClickCell || opts.onClickCell;
		opts.onDblClickCell = opts.oldOnDblClickCell || opts.onDblClickCell;
		opts.onBeforeSelect = opts.OldOnBeforeSelect || opts.onBeforeSelect;
		panel.unbind('.cellediting').find('td.datagrid-row-selected').removeClass('datagrid-row-selected');
		panel.panel('panel').unbind('.cellediting');
	}


	$.extend($.fn.datagrid.methods, {
		editCell: function(jq, param){
			return jq.each(function(){
				editCell(this, param);
			});
		},
		isEditing: function(jq, index){
			var opts = $.data(jq[0], 'datagrid').options;
			var tr = opts.finder.getTr(jq[0], index);
			return tr.length && tr.hasClass('datagrid-row-editing');
		},
		gotoCell: function(jq, param){
			return jq.each(function(){
				gotoCell(this, param);
			});
		},
		enableCellEditing: function(jq){
			return jq.each(function(){
				enableCellEditing(this);
			});
		},
		disableCellEditing: function(jq){
			return jq.each(function(){
				disableCellEditing(this);
			});
		},
		enableCellSelecting: function(jq){
			return jq.each(function(){
				enableCellSelecting(this);
			});
		},
		disableCellSelecting: function(jq){
			return jq.each(function(){
				disableCellSelecting(this);
			});
		},
		input: function(jq, param){
			if (!param){return null;}
			var ed = jq.datagrid('getEditor', param);
			if (ed){
				var t = $(ed.target);
				if (t.hasClass('textbox-f')){
					t = t.textbox('textbox');
				}
				return t;
			} else {
				return null;
			}
		},
		cell: function(jq){		// get current cell info {index,field}
			return getCurrCell(jq[0]);
		},
		getSelectedCells: function(jq){
			return getSelectedCells(jq[0]);
		}
	});

})(jQuery);