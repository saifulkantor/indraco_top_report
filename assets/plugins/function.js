// accounting.js
(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.4.1",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=c.format=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);

$(document).ready(function(){
	$.ajaxSetup({
		data:{contentType:'json'}
	})
});

function formatStatus(val, row) {
	switch (val) {
		case 'p':
			return 'Pending';
			break;
		case 'c':
			return 'Cancel';
			break;
		case 'd':
			return 'Done';
			break;
		default:
			return val;

	}
}

function formatCheck(val, row) {
	if (val == 1) {
		return '<input type="checkbox" checked="checked" onclick="this.checked=!this.checked;"/>';
	} else {
		return '<input type="checkbox" onclick="this.checked=!this.checked;"/>';
	}
}

function formatAction(val, row, index) {
	if (val == 1) {
		return '<a id="btn-hapus" href="javascript:hapusdetail('+index+')" class="easyui-linkbutton l-btn l-btn-small" group=""><span class="l-btn-left"><span class="l-btn-text"><i class="fa fa-trash"></i> <span>Hapus</span></span></span></a>';
	} else {
		return '';
	}
}

// setting datebox
if ($.fn.datebox) {
	/* Y = 2019 */
	/* M = 01-12, m = 1-12, N = Jan-Des */
	/* D = 01-31, d = 1-31 */
	// karakter pemisah tidak boleh huruf
	// set default
	var f = localStorage.getItem("formatTgl");

	if (f == null || f == '') f = 'Y-M-D';

	$.fn.datebox.defaults.format = f;

	$.fn.datebox.defaults.formatter = dateFormat;
	$.fn.datebox.defaults.parser = dateParser;
}
if ($.fn.calendar){
	$.fn.calendar.defaults.weeks = ['Min','Sen','Sel','Rab','Kam','Jum','Sab'];
	$.fn.calendar.defaults.months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
}

function dateFormat(date) {
	date = typeof date !== 'undefined' ? date : new Date();

	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();

	// get format
	var f = $.fn.datebox.defaults.format;

	// replace format tahun
	f = f.replace('Y', y);

	// replace format bulan
	if (f.search('N') >= 0) {
		f = f.replace('N', $.fn.calendar.defaults.months[m-1])
	} else {
		if (f.search('M') >= 0) {
			f = f.replace('M', (m < 10 ? ('0' + m) : m))
		} else if (f.search('m') >= 0) {
			f = f.replace('m', m)
		}
	}

	// replace format hari
	if (f.search('D') >= 0) {
		f = f.replace('D', (d < 10 ? ('0' + d) : d))
	} else if (f.search('d') >= 0) {
		f = f.replace('d', d)
	}

	return f;
}
function dateParser(s) {
	if (!s)
		return new Date();

	// cari delimiter
	var f = $.fn.datebox.defaults.format;

	// hapus huruf d,m,y,n
	var pemisah = f.replace(/d|m|n|y/gi, "");

	pemisah = pemisah[0];

	// split dari parameter pemisah
	var ss = s.split(pemisah);

	// get format
	var f = f.split(pemisah);

	var posisiY = f.indexOf('Y');
	var y = ss[posisiY];

	y = parseInt(y, 10);

	var posisiM = f.indexOf('N');
	if (posisiM >= 0) {
		var m = ss[posisiM];

		// format month to int month
		m = ($.fn.calendar.defaults.months).indexOf(m);
	} else {
		posisiM = ss[f.indexOf('m')];

		if (posisiM >= 0) {
			m = ss[posisiM];
		} else {
			m = ss[f.indexOf('M')];
		}

		m = parseInt(m-1, 10);
	}

	var posisiD = f.indexOf('D');
	if (posisiD >= 0) {
		var d = ss[posisiD];

		d = parseInt(d, 10);
	} else {
		var d = ss[f.indexOf('d')];

		d = parseInt(d, 10);
	}

	if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
		return new Date(y, m, d);
	} else {
		return new Date();
	}
}

// setting numberbox
if ($.fn.numberbox) {
	var p = localStorage.getItem("precisionAmount");
	var g = localStorage.getItem("groupAmount");
	var d = localStorage.getItem("decimalAmount");

	if (p == null || p == '') {
		p = 2;
		g = ',';
		d = '.';
	}

	$.fn.numberbox.defaults.precision = p;
	$.fn.numberbox.defaults.groupSeparator = g;
	$.fn.numberbox.defaults.decimalSeparator = d;
}

// setting formatter view datagrid
function formatAmount(val, row) {
	return accounting.formatNumber(val, $.fn.numberbox.defaults.precision, $.fn.numberbox.defaults.groupSeparator, $.fn.numberbox.defaults.decimalSeparator);
}

// setting formatter view datagrid
function formatDate(val){
	if (!val || val == '' || typeof val == 'undefined')
		return dateFormat();

	// ubah format dari yyyy-mm-dd ke settingan format sesuai program
	var parts = val.split('-');
	// Please pay attention to the month (parts[1]); JavaScript counts months from 0:
	// January - 0, February - 1, etc.
	var mydate = new Date(parts[0], parts[1] - 1, parts[2]);

	// panggil function format milik datebox
	return dateFormat(mydate);
}

// untuk first select combogrid jika data yg diload hanya 1 data
if ($.fn.combogrid) {
	$.fn.combogrid.defaults.selectFirstRow = false;
	$.fn.combogrid.defaults.onLoadSuccess = function(data){
		var dg = $(this);
		var a = dg.datagrid('options');
		var cg = $('#'+a.id);
		if (getType(cg) == 'combogrid') {
			var opt = cg.combogrid('options')
			if (opt.selectFirstRow && opt.mode == 'local') {
				var row = data.rows;
				if (row.length == 1) {
					cg.combogrid('readonly').combogrid('setValue', row[0][opt.idField])
				} else {
					cg.combogrid('readonly', false)
				}
			}
		}
	};
	$.fn.combogrid.defaults.loadFilter = function(data){
		return xssClean(data);
	}
}

// untuk mendapatkan type plugin easyui
function getType(target){
	var plugins = $.parser.plugins;
	for (let i = 0, ln = plugins.length; i < ln; i++) {
		if (target.data(plugins[i])) {
			return plugins[i];
		}
	}
	return null;
}

// untuk mendapatkan editor pada datagrid
function getEditorGrid(dg, i, f) {
	return dg.datagrid('getEditor', {
		index: i,
		field: f
	}).target;
}

// utk antisipasi xss clean
function xssClean(data) {
	var l_data = data;

	l_data = JSON.stringify(l_data);

	l_data = l_data.replace(/</g,"&lt;").replace(/>/g,"&gt;");

	return JSON.parse(l_data);
}

if ($.fn.datagrid) {
	$.fn.datagrid.defaults.loadFilter = function(data){
		return xssClean(data);
	}
}

function cekDuplicate(dg, field)
{
	var data = [];
	var rows = dg.datagrid('getRows')
	var ok = true;
	for (let i = 0, ln = rows.length; i < ln; i++) {
		if (typeof data[rows[i][field]] == 'undefined')
			data[rows[i][field]] = 1;
		else
			data[rows[i][field]]++;

		if (data[rows[i][field]] > 1) {
			$.messager.alert('Warning', 'Item tidak boleh kembar ', 'warning', function(e){
				var fields = dg.datagrid('getColumnFields');
				dg.datagrid('gotoCell', {index:i, field:fields[0]});
			});
			ok = false;
			break;
		}
	}

	return ok;
}
function cekRowDatagrid(dg)
{
	var rows = dg.datagrid('getRows');
	var rowIndex = rows.length - 1;
	var ok = true;
	if (rowIndex > -1) {
		var data = $.map(rows[rowIndex], function (el) {
			return el
		})
		var fields = dg.datagrid('getColumnFields',true).concat(dg.datagrid('getColumnFields'));
		for(let i = 0, ln = fields.length; i < ln; i++) {
			var col = dg.datagrid('getColumnOption', fields[i]);
			var isi = '';
			if (typeof col != 'undefined' && col != 'null') {
				if (typeof col.editor != 'undefined' && col.editor != 'null') {
					if (typeof col.editor.options != 'undefined' && col.editor.options != 'null') {
						if (col.editor.options.required && data.length > 0) {
							if (rows[rowIndex][fields[i]] === '') {
								$.messager.alert('Warning', 'Kolom ' + col.title + ' belum diisi...','warning', function(e){
									dg.datagrid('gotoCell', {
										index: rowIndex,
										field: fields[i]
									});
								});
								ok = false;
								break;
							}
						}
					}
				}
			}
		}
	}

	return ok;
}

// untuk menghasilkan styler pada datagrid
function styleMaster(index,row){
	if (row.aktif == 0){
		return {class:'master-inaktif'};
	}
}

function styleData(index,row){
	if (row.status == 'S'){
		return {class:'data-slip'};
	} else if (row.status == 'P'){
		return {class:'data-post'};
	} else if (row.status == 'D'){
		return {class:'data-delete'};
	}
}

// untuk membuat tombol simpan tidak dobel klik
function setClick(target){
	var clickHandler = function(e){
		var opts = $(this).linkbutton('options');
		if (!opts.disabled){
			if (opts.toggle){
				if (opts.selected){
					$(this).linkbutton('unselect');
				} else {
					$(this).linkbutton('select');
				}
			}
			opts.onClick.call(this);

			$(target).unbind('click.linkbutton');
			$(target).linkbutton('disable');
			$(target).removeClass('l-btn-disabled l-btn-plain-disabled');
			setTimeout(function(){
				$(target).linkbutton('enable');
				$(target).unbind('click.linkbutton').bind('click.linkbutton', clickHandler);
			}, 1000);
		}
	};
	$(target).unbind('click.linkbutton').bind('click.linkbutton', clickHandler);
}
$.extend($.fn.linkbutton.methods, {
	singleClick: function(jq){
		return jq.each(function(){
			setClick(this);
		});
	}
})
